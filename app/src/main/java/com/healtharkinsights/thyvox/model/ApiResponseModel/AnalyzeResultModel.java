package com.healtharkinsights.thyvox.model.ApiResponseModel;

/**
 * {@code }
 *
 * @author Romi Chandra
 * @version 1.0
 * @since 7/22/18
 */
public class AnalyzeResultModel {
    private String status;
    private double probThyroid;
    private double probNormal;
    private String analysis;
    private String comment;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public double getProbThyroid() {
        return probThyroid;
    }

    public void setProbThyroid(double probThyroid) {
        this.probThyroid = probThyroid;
    }

    public double getProbNormal() {
        return probNormal;
    }

    public void setProbNormal(double probNormal) {
        this.probNormal = probNormal;
    }

    public String getAnalysis() {
        return analysis;
    }

    public void setAnalysis(String analysis) {
        this.analysis = analysis;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
