package com.healtharkinsights.thyvox.model.ApiResponseModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SaveSampleModel {

    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }


    public class Data {

        @SerializedName("insertId")
        @Expose
        private Integer insertId;

        public Integer getInsertId() {
            return insertId;
        }

        public void setInsertId(Integer insertId) {
            this.insertId = insertId;
        }

    }

}