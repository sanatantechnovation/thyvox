package com.healtharkinsights.thyvox.model.ApiResponseModel;

/**
 * {@code }
 *
 * @author Romi Chandra
 * @version 1.0
 * @since 5/27/18
 */
public class SaveSample {
    String message;
    SaveSampleData sampleData;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public SaveSampleData getSampleData() {
        return sampleData;
    }

    public void setSampleData(SaveSampleData sampleData) {
        this.sampleData = sampleData;
    }
}
