package com.healtharkinsights.thyvox.model;

import org.json.JSONObject;

import java.util.Date;
import java.util.List;

import com.healtharkinsights.thyvox.helper.AppSharedPreferences;
import com.healtharkinsights.thyvox.helper.Validator;

/**
 * {@code }
 *
 * @author Romi Chandra
 * @version 1.0
 * @since 5/8/18
 */
public class Sample {

    private int id;
    private String name;
    private Date dob;
    private String gender;
    private String cholestrol;
    private String t3;
    private String t4;
    private String tsh;




    private String subjectDetails;
    private String sampleDetails;
    private String symptomDetails;

    private float weight;
    private int heightFT;
    private int heightIN;
    private String bpHigh, bpLow;

    private boolean isSufferingFromWeightGain;
    private boolean isSufferingFromWeightLoss;
    private boolean isSufferingFromFatigue;
    private boolean isSufferingFromFeelingCold;
    private boolean isSufferingFromWeakness;
    private boolean isSufferingFromHairLoss;

    private List<Recording> recordings;
    private String recordsFilePath;
    private String time;

    private int birthDay;
    private int birthMonth;
    private int birthYear;

    private String dateAdded;
    private String dateModified;
    private int subjectId;
    private String email;
    private String mobile;


    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    private String userID;




    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }

    public int getHeightFT() {
        return heightFT;
    }

    public void setHeightFT(int heightFT) {
        this.heightFT = heightFT;
    }

    public int getHeightIN() {
        return heightIN;
    }

    public void setHeightIN(int heightIN) {
        this.heightIN = heightIN;
    }

    public String getBpHigh() {
        return bpHigh;
    }

    public void setBpHigh(String bpHigh) {
        this.bpHigh = bpHigh;
    }

    public String getBpLow() {
        return bpLow;
    }

    public void setBpLow(String bpLow) {
        this.bpLow = bpLow;
    }

    public String getCholestrol() {
        return cholestrol;
    }

    public void setCholestrol(String cholestrol) {
        this.cholestrol = cholestrol;
    }

    public String getT3() {
        return t3;
    }

    public void setT3(String t3) {
        this.t3 = t3;
    }

    public String getT4() {
        return t4;
    }

    public void setT4(String t4) {
        this.t4 = t4;
    }

    public String getTsh() {
        return tsh;
    }

    public void setTsh(String tsh) {
        this.tsh = tsh;
    }

    public boolean isSufferingFromWeightGain() {
        return isSufferingFromWeightGain;
    }

    public void setSufferingFromWeightGain(boolean sufferingFromWeightGain) {
        isSufferingFromWeightGain = sufferingFromWeightGain;
    }

    public boolean isSufferingFromWeightLoss() {
        return isSufferingFromWeightLoss;
    }

    public void setSufferingFromWeightLoss(boolean sufferingFromWeightLoss) {
        isSufferingFromWeightLoss = sufferingFromWeightLoss;
    }

    public boolean isSufferingFromFatigue() {
        return isSufferingFromFatigue;
    }

    public void setSufferingFromFatigue(boolean sufferingFromFatigue) {
        isSufferingFromFatigue = sufferingFromFatigue;
    }

    public boolean isSufferingFromFeelingCold() {
        return isSufferingFromFeelingCold;
    }

    public void setSufferingFromFeelingCold(boolean sufferingFromFeelingCold) {
        isSufferingFromFeelingCold = sufferingFromFeelingCold;
    }

    public boolean isSufferingFromWeakness() {
        return isSufferingFromWeakness;
    }

    public void setSufferingFromWeakness(boolean sufferingFromWeakness) {
        isSufferingFromWeakness = sufferingFromWeakness;
    }

    public boolean isSufferingFromHairLoss() {
        return isSufferingFromHairLoss;
    }

    public void setSufferingFromHairLoss(boolean sufferingFromHairLoss) {
        isSufferingFromHairLoss = sufferingFromHairLoss;
    }

    public List<Recording> getRecordings() {
        return recordings;
    }

    public void setRecordings(List<Recording> recordings) {
        this.recordings = recordings;
    }

    public String getRecordsFilePath() {
        return recordsFilePath;
    }

    public void setRecordsFilePath(String recordsFilePath) {
        this.recordsFilePath = recordsFilePath;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(int birthDay) {
        this.birthDay = birthDay;
    }

    public int getBirthMonth() {
        return birthMonth;
    }

    public void setBirthMonth(int birthMonth) {
        this.birthMonth = birthMonth;
    }

    public int getBirthYear() {
        return birthYear;
    }

    public void setBirthYear(int birthYear) {
        this.birthYear = birthYear;
    }

    public String getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(String dateAdded) {
        this.dateAdded = dateAdded;
    }

    public int getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(int subjectId) {
        this.subjectId = subjectId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getDateModified() {
        return dateModified;
    }

    public void setDateModified(String dateModified) {
        this.dateModified = dateModified;
    }


    public String getSubjectDetails() {
        try {
            JSONObject jsonProfile = new JSONObject();
            jsonProfile.put("user_id",this.userID);
            jsonProfile.put("name", this.name == null ? "" : this.name);
            jsonProfile.put("email", "");
            jsonProfile.put("mobile", 0000000000);
            jsonProfile.put("gender", this.gender == null ? "" : this.gender);
            jsonProfile.put("dob", String.valueOf(this.birthYear) + "-" + String.valueOf(this.birthMonth) + "-" + String.valueOf(this.birthDay));
            subjectDetails = jsonProfile.toString();

        }catch (Exception e){
            e.printStackTrace();
        }

        return subjectDetails;
    }

    public void setSubjectDetails(String subjectDetails) {
        this.subjectDetails = subjectDetails;
    }

    public String getSampleDetails() {
        try {
            JSONObject jsonSample = new JSONObject();
            jsonSample.put("bpHigh", this.bpHigh);
            jsonSample.put("bpLow", this.bpLow);
            jsonSample.put("cholestrol", this.cholestrol == null ? "" : this.cholestrol);
            jsonSample.put("height", this.heightFT == Validator.INVALID_SAMPLE_FIELD_INT_VALUE ? Validator.INVALID_SAMPLE_FIELD_INT_VALUE : Integer.toString(this.heightFT) + "ft " + Integer.toString(this.heightIN) + "in");
            jsonSample.put("weight", this.weight == Validator.INVALID_SAMPLE_FIELD_FLOAT_VALUE ? Validator.INVALID_SAMPLE_FIELD_FLOAT_VALUE : Float.toString(this.weight));
            jsonSample.put("t3", this.t3 == null ? "" : this.t3);
            jsonSample.put("t4", this.t4 == null ? "" : this.t4);
            jsonSample.put("tsh", this.tsh == null ? "" : this.tsh);
            sampleDetails = jsonSample.toString();
        }catch (Exception e){
            e.printStackTrace();
        }
        return sampleDetails;
    }

    public void setSampleDetails(String sampleDetails) {
        this.sampleDetails = sampleDetails;
    }

    public String getSymptomDetails() {

        try {
            JSONObject jsonSymptoms = new JSONObject();
            jsonSymptoms.put("RWG", this.isSufferingFromWeightGain);
            jsonSymptoms.put("RWL", this.isSufferingFromWeightLoss);
            jsonSymptoms.put("TIR", this.isSufferingFromFatigue);
            jsonSymptoms.put("CLD", this.isSufferingFromFeelingCold);
            jsonSymptoms.put("WEA", this.isSufferingFromWeakness);
            jsonSymptoms.put("HAL", this.isSufferingFromHairLoss);
            symptomDetails = jsonSymptoms.toString();
        }catch (Exception e){
            e.printStackTrace();
        }

        return symptomDetails;
    }

    public void setSymptomDetails(String symptomDetails) {
        this.symptomDetails = symptomDetails;
    }


    public JSONObject toJSON() {
        JSONObject jsonObject = new JSONObject();
        try {

            jsonObject.put("user_id",this.userID);
            jsonObject.put("token",this.token);


            JSONObject jsonProfile = new JSONObject();
            jsonProfile.put("user_id",this.userID);
            jsonProfile.put("name", this.name == null ? "" : this.name);
            jsonProfile.put("email", "");
            jsonProfile.put("mobile", 0000000000);
            jsonProfile.put("gender", this.gender == null ? "" : this.gender);
            jsonProfile.put("dob", String.valueOf(this.birthYear) + "-" + String.valueOf(this.birthMonth) + "-" + String.valueOf(this.birthDay));

            JSONObject jsonSample = new JSONObject();
            jsonSample.put("bpHigh", this.bpHigh);
            jsonSample.put("bpLow", this.bpLow);
            jsonSample.put("cholestrol", this.cholestrol == null ? "" : this.cholestrol);
            jsonSample.put("height", this.heightFT == Validator.INVALID_SAMPLE_FIELD_INT_VALUE ? Validator.INVALID_SAMPLE_FIELD_INT_VALUE : Integer.toString(this.heightFT) + "ft " + Integer.toString(this.heightIN) + "in");
            jsonSample.put("weight", this.weight == Validator.INVALID_SAMPLE_FIELD_FLOAT_VALUE ? Validator.INVALID_SAMPLE_FIELD_FLOAT_VALUE : Float.toString(this.weight));
            jsonSample.put("t3", this.t3 == null ? "" : this.t3);
            jsonSample.put("t4", this.t4 == null ? "" : this.t4);
            jsonSample.put("tsh", this.tsh == null ? "" : this.tsh);

            JSONObject jsonSymptoms = new JSONObject();
            jsonSymptoms.put("RWG", this.isSufferingFromWeightGain);
            jsonSymptoms.put("RWL", this.isSufferingFromWeightLoss);
            jsonSymptoms.put("TIR", this.isSufferingFromFatigue);
            jsonSymptoms.put("CLD", this.isSufferingFromFeelingCold);
            jsonSymptoms.put("WEA", this.isSufferingFromWeakness);
            jsonSymptoms.put("HAL", this.isSufferingFromHairLoss);

            jsonObject.put("subject_details", jsonProfile);
            jsonObject.put("sampleDetails", jsonSample);
            jsonObject.put("symptomDetails", jsonSymptoms);


        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonObject;
    }
}
