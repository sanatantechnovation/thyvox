package com.healtharkinsights.thyvox.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SubjectSampleModel {

    @SerializedName("isError")
    @Expose
    private Boolean isError;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;

    public Boolean getIsError() {
        return isError;
    }

    public void setIsError(Boolean isError) {
        this.isError = isError;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }


    public class Data {

        @SerializedName("result")
        @Expose
        private Result result;

        public Result getResult() {
            return result;
        }

        public void setResult(Result result) {
            this.result = result;
        }

    }

    public class Result {

        @SerializedName("subject_id")
        @Expose
        private String subjectId;
        @SerializedName("filename")
        @Expose
        private String filename;
        @SerializedName("analysis")
        @Expose
        private Object analysis;
        @SerializedName("comments")
        @Expose
        private Object comments;
        @SerializedName("access_url")
        @Expose
        private String accessUrl;
        @SerializedName("ProbNormal")
        @Expose
        private Object probNormal;
        @SerializedName("ProbThyroid")
        @Expose
        private Object probThyroid;

        public String getSubjectId() {
            return subjectId;
        }

        public void setSubjectId(String subjectId) {
            this.subjectId = subjectId;
        }

        public String getFilename() {
            return filename;
        }

        public void setFilename(String filename) {
            this.filename = filename;
        }

        public Object getAnalysis() {
            return analysis;
        }

        public void setAnalysis(Object analysis) {
            this.analysis = analysis;
        }

        public Object getComments() {
            return comments;
        }

        public void setComments(Object comments) {
            this.comments = comments;
        }

        public String getAccessUrl() {
            return accessUrl;
        }

        public void setAccessUrl(String accessUrl) {
            this.accessUrl = accessUrl;
        }

        public Object getProbNormal() {
            return probNormal;
        }

        public void setProbNormal(Object probNormal) {
            this.probNormal = probNormal;
        }

        public Object getProbThyroid() {
            return probThyroid;
        }

        public void setProbThyroid(Object probThyroid) {
            this.probThyroid = probThyroid;
        }

    }

}
