package com.healtharkinsights.thyvox.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UploadAudioModel {

    @SerializedName("isError")
    @Expose
    private Boolean isError;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;

    public Boolean getIsError() {
        return isError;
    }

    public void setIsError(Boolean isError) {
        this.isError = isError;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }


    public class Data {

        @SerializedName("subject_id")
        @Expose
        private String subjectId;
        @SerializedName("audio_id1")
        @Expose
        private Integer audioId1;
        @SerializedName("filename1")
        @Expose
        private String filename1;
        @SerializedName("audio_id2")
        @Expose
        private Integer audioId2;
        @SerializedName("filename2")
        @Expose
        private String filename2;
        @SerializedName("audio_id3")
        @Expose
        private Integer audioId3;
        @SerializedName("filename3")
        @Expose
        private String filename3;

        public String getSubjectId() {
            return subjectId;
        }

        public void setSubjectId(String subjectId) {
            this.subjectId = subjectId;
        }

        public Integer getAudioId1() {
            return audioId1;
        }

        public void setAudioId1(Integer audioId1) {
            this.audioId1 = audioId1;
        }

        public String getFilename1() {
            return filename1;
        }

        public void setFilename1(String filename1) {
            this.filename1 = filename1;
        }

        public Integer getAudioId2() {
            return audioId2;
        }

        public void setAudioId2(Integer audioId2) {
            this.audioId2 = audioId2;
        }

        public String getFilename2() {
            return filename2;
        }

        public void setFilename2(String filename2) {
            this.filename2 = filename2;
        }

        public Integer getAudioId3() {
            return audioId3;
        }

        public void setAudioId3(Integer audioId3) {
            this.audioId3 = audioId3;
        }

        public String getFilename3() {
            return filename3;
        }

        public void setFilename3(String filename3) {
            this.filename3 = filename3;
        }

    }

}
