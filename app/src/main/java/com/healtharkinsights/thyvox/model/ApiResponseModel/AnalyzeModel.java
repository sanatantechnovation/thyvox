package com.healtharkinsights.thyvox.model.ApiResponseModel;

public class AnalyzeModel {


    public String analysis;

    public String getAnalysis() {
        return analysis;
    }

    public void setAnalysis(String analysis) {
        this.analysis = analysis;
    }

    public Double getProbNormal() {
        return probNormal;
    }

    public void setProbNormal(Double probNormal) {
        this.probNormal = probNormal;
    }

    public Double getProbThyroid() {
        return probThyroid;
    }

    public void setProbThyroid(Double probThyroid) {
        this.probThyroid = probThyroid;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public Double probNormal;
    public Double probThyroid;
    public String comment;
    public String file;



}
