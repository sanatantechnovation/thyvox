package com.healtharkinsights.thyvox.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RecordingInfoModel {

    @SerializedName("isError")
    @Expose
    private Boolean isError;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;

    public Boolean getIsError() {
        return isError;
    }

    public void setIsError(Boolean isError) {
        this.isError = isError;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data {

        @SerializedName("subject")
        @Expose
        private Subject subject;
        @SerializedName("symptoms")
        @Expose
        private Symptoms symptoms;
        @SerializedName("files")
        @Expose
        private List<File> files = null;

        public Subject getSubject() {
            return subject;
        }

        public void setSubject(Subject subject) {
            this.subject = subject;
        }

        public Symptoms getSymptoms() {
            return symptoms;
        }

        public void setSymptoms(Symptoms symptoms) {
            this.symptoms = symptoms;
        }

        public List<File> getFiles() {
            return files;
        }

        public void setFiles(List<File> files) {
            this.files = files;
        }

    }

    public class File {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("subject_id")
        @Expose
        private String subjectId;
        @SerializedName("filename")
        @Expose
        private String filename;
        @SerializedName("duration")
        @Expose
        private Object duration;
        @SerializedName("analysis")
        @Expose
        private String analysis;
        @SerializedName("comments")
        @Expose
        private String comments;
        @SerializedName("access_url")
        @Expose
        private String accessUrl;
        @SerializedName("date_added")
        @Expose
        private String dateAdded;
        @SerializedName("ProbNormal")
        @Expose
        private String probNormal;
        @SerializedName("ProbThyroid")
        @Expose
        private String probThyroid;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getSubjectId() {
            return subjectId;
        }

        public void setSubjectId(String subjectId) {
            this.subjectId = subjectId;
        }

        public String getFilename() {
            return filename;
        }

        public void setFilename(String filename) {
            this.filename = filename;
        }

        public Object getDuration() {
            return duration;
        }

        public void setDuration(Object duration) {
            this.duration = duration;
        }

        public String getAnalysis() {
            return analysis;
        }

        public void setAnalysis(String analysis) {
            this.analysis = analysis;
        }

        public String getComments() {
            return comments;
        }

        public void setComments(String comments) {
            this.comments = comments;
        }

        public String getAccessUrl() {
            return accessUrl;
        }

        public void setAccessUrl(String accessUrl) {
            this.accessUrl = accessUrl;
        }

        public String getDateAdded() {
            return dateAdded;
        }

        public void setDateAdded(String dateAdded) {
            this.dateAdded = dateAdded;
        }

        public String getProbNormal() {
            return probNormal;
        }

        public void setProbNormal(String probNormal) {
            this.probNormal = probNormal;
        }

        public String getProbThyroid() {
            return probThyroid;
        }

        public void setProbThyroid(String probThyroid) {
            this.probThyroid = probThyroid;
        }

    }

    public class Subject {

        @SerializedName("_id")
        @Expose
        private String id;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("mobile")
        @Expose
        private String mobile;
        @SerializedName("dob")
        @Expose
        private String dob;
        @SerializedName("gender")
        @Expose
        private String gender;
        @SerializedName("status")
        @Expose
        private Object status;
        @SerializedName("date_added")
        @Expose
        private String dateAdded;
        @SerializedName("user_id")
        @Expose
        private Object userId;
        @SerializedName("subject_id")
        @Expose
        private String subjectId;
        @SerializedName("height")
        @Expose
        private String height;
        @SerializedName("weight")
        @Expose
        private String weight;
        @SerializedName("bpHigh")
        @Expose
        private String bpHigh;
        @SerializedName("bpLow")
        @Expose
        private String bpLow;
        @SerializedName("cholestrol")
        @Expose
        private String cholestrol;
        @SerializedName("t3")
        @Expose
        private String t3;
        @SerializedName("t4")
        @Expose
        private String t4;
        @SerializedName("tsh")
        @Expose
        private String tsh;
        @SerializedName("date_modified")
        @Expose
        private Object dateModified;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getDob() {
            return dob;
        }

        public void setDob(String dob) {
            this.dob = dob;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public Object getStatus() {
            return status;
        }

        public void setStatus(Object status) {
            this.status = status;
        }

        public String getDateAdded() {
            return dateAdded;
        }

        public void setDateAdded(String dateAdded) {
            this.dateAdded = dateAdded;
        }

        public Object getUserId() {
            return userId;
        }

        public void setUserId(Object userId) {
            this.userId = userId;
        }

        public String getSubjectId() {
            return subjectId;
        }

        public void setSubjectId(String subjectId) {
            this.subjectId = subjectId;
        }

        public String getHeight() {
            return height;
        }

        public void setHeight(String height) {
            this.height = height;
        }

        public String getWeight() {
            return weight;
        }

        public void setWeight(String weight) {
            this.weight = weight;
        }

        public String getBpHigh() {
            return bpHigh;
        }

        public void setBpHigh(String bpHigh) {
            this.bpHigh = bpHigh;
        }

        public String getBpLow() {
            return bpLow;
        }

        public void setBpLow(String bpLow) {
            this.bpLow = bpLow;
        }

        public String getCholestrol() {
            return cholestrol;
        }

        public void setCholestrol(String cholestrol) {
            this.cholestrol = cholestrol;
        }

        public String getT3() {
            return t3;
        }

        public void setT3(String t3) {
            this.t3 = t3;
        }

        public String getT4() {
            return t4;
        }

        public void setT4(String t4) {
            this.t4 = t4;
        }

        public String getTsh() {
            return tsh;
        }

        public void setTsh(String tsh) {
            this.tsh = tsh;
        }

        public Object getDateModified() {
            return dateModified;
        }

        public void setDateModified(Object dateModified) {
            this.dateModified = dateModified;
        }

    }

    public class Symptoms {

        @SerializedName("subject_id")
        @Expose
        private String subjectId;
        @SerializedName("RWG")
        @Expose
        private String rWG;
        @SerializedName("RWL")
        @Expose
        private String rWL;
        @SerializedName("TIR")
        @Expose
        private String tIR;
        @SerializedName("CLD")
        @Expose
        private String cLD;
        @SerializedName("WEA")
        @Expose
        private String wEA;
        @SerializedName("HAL")
        @Expose
        private String hAL;

        public String getSubjectId() {
            return subjectId;
        }

        public void setSubjectId(String subjectId) {
            this.subjectId = subjectId;
        }

        public String getRWG() {
            return rWG;
        }

        public void setRWG(String rWG) {
            this.rWG = rWG;
        }

        public String getRWL() {
            return rWL;
        }

        public void setRWL(String rWL) {
            this.rWL = rWL;
        }

        public String getTIR() {
            return tIR;
        }

        public void setTIR(String tIR) {
            this.tIR = tIR;
        }

        public String getCLD() {
            return cLD;
        }

        public void setCLD(String cLD) {
            this.cLD = cLD;
        }

        public String getWEA() {
            return wEA;
        }

        public void setWEA(String wEA) {
            this.wEA = wEA;
        }

        public String getHAL() {
            return hAL;
        }

        public void setHAL(String hAL) {
            this.hAL = hAL;
        }

    }

}