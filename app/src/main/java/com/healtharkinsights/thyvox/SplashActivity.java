package com.healtharkinsights.thyvox;

import android.animation.ObjectAnimator;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.drawable.TransitionDrawable;
import android.os.Build;
import android.os.Handler;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.ImageView;

import com.healtharkinsights.thyvox.helper.AppSharedPreferences;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);
        getSupportActionBar().hide();
        ConstraintLayout view = findViewById(R.id.splashBackground);

        ImageView image = findViewById(R.id.splashImage);

        ObjectAnimator anim = ObjectAnimator.ofFloat(image, "Alpha", 0, 1);
        anim.setDuration(1500);
        anim.start();

        final TransitionDrawable background = (TransitionDrawable) view.getBackground();
        background.startTransition(1000);

        //TODO don't forgot to set Run-Time permission..
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
               if (AppSharedPreferences.getInstance().getUserLogin()==1){
                   Intent intent = new Intent(SplashActivity.this, AboutActivity.class);
                   intent.putExtra("showButton",true);
                   finish();
                   startActivity(intent);
                   overridePendingTransition(R.anim.trans_left_in, R.anim.trans_left_out);

               }else {
                   Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                   finish();
                   startActivity(intent);
                   overridePendingTransition(R.anim.trans_left_in, R.anim.trans_left_out);
               }

            }
        }, 1500);
    }
}
