package com.healtharkinsights.thyvox;

import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;



import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;


import az.plainpie.PieView;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import com.healtharkinsights.thyvox.helper.AppSharedPreferences;
import com.healtharkinsights.thyvox.helper.Constant;
import com.healtharkinsights.thyvox.helper.Utility;


public class MyAnalyzeResultActivity extends AppCompatActivity {
    private String strInsertID = "",strFrom="";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.WHITE);
        }
        setContentView(R.layout.activity_my_analyze_result);


        clearResultPref();


        if (getIntent().hasExtra("InsertID")) {
            strInsertID = getIntent().getStringExtra("InsertID");
            strFrom = getIntent().getStringExtra("From");
            Log.e( "MyAnalyzeResult: ", " strInsertID : "+strInsertID);
            Log.e( "MyAnalyzeResult: ", " strFrom : "+strFrom);



        }


        for (int i = 0; i < 3; i++) {
            if (i==0){
                if (strFrom.contains("InfoDetailsActivity")){
                    analyzeFile(String.valueOf(AppSharedPreferences.getInstance().getUploadAudioModel().getData().getFilename1()), strInsertID, i,AppSharedPreferences.getInstance().getUploadAudioModel().getData().getAudioId1());
                }else {
                    analyzeFile(String.valueOf(AppSharedPreferences.getInstance().getRecordingInfoModel().getData().getFiles().get(0).getFilename()), strInsertID, i, Integer.parseInt(AppSharedPreferences.getInstance().getRecordingInfoModel().getData().getFiles().get(0).getId()));
                }
            }else if (i==1){
                if (strFrom.contains("InfoDetailsActivity")){
                    analyzeFile(String.valueOf(AppSharedPreferences.getInstance().getUploadAudioModel().getData().getFilename2()), strInsertID, i,AppSharedPreferences.getInstance().getUploadAudioModel().getData().getAudioId2());
                }else {
                    analyzeFile(String.valueOf(AppSharedPreferences.getInstance().getRecordingInfoModel().getData().getFiles().get(1).getFilename()), strInsertID, i,Integer.parseInt(AppSharedPreferences.getInstance().getRecordingInfoModel().getData().getFiles().get(1).getId()));

                }
            }else if (i==2){
                if (strFrom.contains("InfoDetailsActivity")){
                    analyzeFile(String.valueOf(AppSharedPreferences.getInstance().getUploadAudioModel().getData().getFilename3()), strInsertID, i,AppSharedPreferences.getInstance().getUploadAudioModel().getData().getAudioId3());
                }else {
                    analyzeFile(String.valueOf(AppSharedPreferences.getInstance().getRecordingInfoModel().getData().getFiles().get(2).getFilename()), strInsertID, i,Integer.parseInt(AppSharedPreferences.getInstance().getRecordingInfoModel().getData().getFiles().get(2).getId()));

                }
            }
        }

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        Intent intent = new Intent(this, AnalyzeResultActivity.class);
        intent.putExtra("InsertID",strInsertID);
        startActivity(intent);
        overridePendingTransition(0,0);


    }


    private void analyzeFile(final String fileName, final String sampleID, final int index, final int strAudioID) {
        OkHttpClient client = new OkHttpClient();
        Request.Builder builder = new Request.Builder().url(Constant.BASE_URL+"sample_analyze/"+sampleID+"/"+fileName+"/"+strAudioID);
        Log.e("analyzeFile", "File analyze request url : "+Constant.BASE_URL+"sample_analyze/"+sampleID+"/"+fileName+"/"+strAudioID);
        Request request = builder.build();

        try {
            Call call = client.newCall(request);
            call.enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    Utility.myProgressBar.hideProgressBar();

                }

                @Override
                public void onResponse(Call call, final Response response) throws IOException {
                    final String result = response.body().string();
                    Utility.myProgressBar.hideProgressBar();


                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            Utility.myProgressBar.hideProgressBar();
        }
    }




    @Override
    public void onBackPressed() {
        clearResultPref();
        Intent intent = new Intent(MyAnalyzeResultActivity.this, HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        finish();
        startActivity(intent);
        overridePendingTransition(R.anim.trans_left_in, R.anim.trans_left_out);
    }


    private void clearResultPref(){
        AppSharedPreferences.getInstance().setFirstAnalyze("");
        AppSharedPreferences.getInstance().setFirstProThyropid(0);
        AppSharedPreferences.getInstance().setSecondAnalyze("");
        AppSharedPreferences.getInstance().setSecondProThyropid(0);
        AppSharedPreferences.getInstance().setThirdAnalyze("");
        AppSharedPreferences.getInstance().setThirdProThyropid(0);
    }







}

