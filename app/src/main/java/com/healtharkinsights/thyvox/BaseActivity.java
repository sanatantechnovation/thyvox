package com.healtharkinsights.thyvox;

import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.healtharkinsights.thyvox.helper.AppSharedPreferences;
import com.healtharkinsights.thyvox.helper.Constant;
import com.healtharkinsights.thyvox.helper.ApiCallFunction;
import com.healtharkinsights.thyvox.helper.Utility;

import java.util.HashMap;


public class BaseActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private DrawerLayout drawer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.WHITE);
        }
        setContentView(R.layout.activity_base);

        drawer = findViewById(R.id.drawer_layout);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, null, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        navigationView.getHeaderView(0).findViewById(R.id.profile_image).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawer.isDrawerOpen(Gravity.START)) {
                    drawer.closeDrawer(Gravity.START);
                }
            }
        });

        TextView tvEmail = navigationView.getHeaderView(0).findViewById(R.id.tvEmail);
        TextView tvName = navigationView.getHeaderView(0).findViewById(R.id.tvName);
        tvEmail.setText(AppSharedPreferences.getInstance().getUserMail());
        tvName.setText(AppSharedPreferences.getInstance().getUserName());
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
           /* case R.id.nav_home:
                break;*/
           /* case R.id.nav_profile:
                break;*/
            case R.id.nav_my_recording:
                Intent i = new Intent(this, SavedRecordsActivity.class);
                finish();
                startActivity(i);
                overridePendingTransition(R.anim.trans_left_in, R.anim.trans_left_out);
                break;
            case R.id.nav_about:
                Intent intent = new Intent(this, AboutActivity.class);
                intent.putExtra("showButton",false);
                finish();
                startActivity(intent);
                overridePendingTransition(R.anim.trans_left_in, R.anim.trans_left_out);

                break;
            case R.id.nav_logout:
                new LogOut().execute();
                break;
        }
        drawer.closeDrawer(GravityCompat.START);
        return false;
    }


    public void openDrawer() {
        if (drawer.isDrawerOpen(Gravity.START)) {
            drawer.closeDrawer(Gravity.START);
        } else {
            drawer.openDrawer(Gravity.START);
        }

    }

    public void hideDrawer() {
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
    }


    public class LogOut extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            Utility.myProgressBar.showProgressBar(BaseActivity.this);
        }

        @Override
        protected void onPostExecute(String response) {
            Utility.myProgressBar.hideProgressBar();
        }

        @Override
        protected String doInBackground(String... params) {
            HashMap<String, String> parameters = new HashMap<>();
            parameters.put("user_id",AppSharedPreferences.getInstance().getUserId());
            parameters.put("token", AppSharedPreferences.getInstance().getTokenValue());
            Log.e("BaseActivity ", "logout_user parameters : " + parameters);

            String isError = "",response = "";
            response = ApiCallFunction.postRequest(BaseActivity.this, Constant.BASE_URL+"logout_user", parameters);
            Log.e( "LogOut: ", "response : " +response);
            if (response.length() > 0) {
                Utility.myProgressBar.hideProgressBar();
                AppSharedPreferences.getInstance().setUserLogin(0);
                AppSharedPreferences.getInstance().clearPreference(BaseActivity.this);
                Intent intent = new Intent(BaseActivity.this, LoginActivity.class);
                finish();
                startActivity(intent);
                overridePendingTransition(R.anim.trans_left_in, R.anim.trans_left_out);

            }
            return isError;
        }
    }


}
