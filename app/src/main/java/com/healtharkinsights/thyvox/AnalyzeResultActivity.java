package com.healtharkinsights.thyvox;

import android.animation.ValueAnimator;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import java.io.IOException;
import java.text.DecimalFormat;
import az.plainpie.PieView;
import az.plainpie.animation.PieAngleAnimation;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.healtharkinsights.thyvox.helper.Constant;
import com.healtharkinsights.thyvox.helper.Utility;
import com.healtharkinsights.thyvox.model.SubjectSampleModel;

public class AnalyzeResultActivity extends AppCompatActivity {

    String strInsertID;

    private String fileNameArray = "", analysis = "", comment = "";
    double probT = 0;
    private PieView pieView;
    private TextView pieText, textComment, textAnalyze, textAnalysisValueProbT;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.WHITE);
        }
        setContentView(R.layout.activity_analyze_result);


        if (getIntent().hasExtra("InsertID")) {
            strInsertID = getIntent().getStringExtra("InsertID");
        }

        Log.e( "onCreate: "," strInsertID : "+strInsertID );

        pieView = findViewById(R.id.pieView);
        pieText = findViewById(R.id.pieText);
        textComment = findViewById(R.id.textAnalysisComment);
        textAnalyze = findViewById(R.id.textAnalysis);
        textAnalysisValueProbT = findViewById(R.id.textAnalysisValueProbT);

        ImageButton backButton = findViewById(R.id.toolBarButtonBack);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();

            }
        });

        new SubjectSamples().execute(strInsertID);


    }


    public class SubjectSamples extends AsyncTask<String, String, String> {
        String isError = "";


        @Override
        protected void onPreExecute() {
            Utility.myProgressBar.showProgressBar(AnalyzeResultActivity.this);
        }

        @Override
        protected void onPostExecute(String response) {
            Utility.myProgressBar.hideProgressBar();

        }

        @Override
        protected String doInBackground(String... params) {
            OkHttpClient client = new OkHttpClient();
            Request.Builder builder = new Request.Builder().url(Constant.BASE_URL+"subject_samples/"+params[0]);
            ImageButton backButton = findViewById(R.id.toolBarButtonBack);

            Log.e("SubjectSamples", "SubjectSamples request url : "+Constant.BASE_URL+"subject_samples/"+params[0]);
            Request request = builder.build();

            try {
                Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {
                        Log.e("SubjectSamples", "subject_samples, error : " + e.toString());
                        Utility.myProgressBar.hideProgressBar();

                    }

                    @Override
                    public void onResponse(Call call, Response response) throws IOException {
                        final String result = response.body().string();

                        Log.e("SubjectSamples", "Subject_samples, onResponse : " + result);
                        SubjectSampleModel subjectSamples = new Gson().fromJson(result, new TypeToken<SubjectSampleModel>() {}.getType());


                        if (subjectSamples.getData().getResult().getAnalysis()!=null)
                        analysis = String.valueOf(subjectSamples.getData().getResult().getAnalysis());
                        if (subjectSamples.getData().getResult().getComments()!=null)
                        comment = String.valueOf(subjectSamples.getData().getResult().getComments());
                        if (subjectSamples.getData().getResult().getProbThyroid()!=null){
                            double mNewProThyroid = Double.parseDouble(String.valueOf(subjectSamples.getData().getResult().getProbThyroid()));
                            probT = mNewProThyroid * 100;
                        }
                        DecimalFormat df2 = new DecimalFormat("#.##");
                        probT = Double.valueOf(df2.format(probT));


                        runOnUiThread(new Runnable() {
                            public void run() {
                                if (analysis == null) {
                                    textComment.setText("No Results Found!");
                                } else {
                                    if (analysis.equals("THYROID")) {
                                        pieView.setPercentageBackgroundColor(getResources().getColor(R.color.colorRed));
                                        textAnalysisValueProbT.setTextColor(getResources().getColor(R.color.colorRed));
                                    } else {
                                        pieView.setPercentageBackgroundColor(getResources().getColor(R.color.colorDarkGreen));
                                        textAnalysisValueProbT.setTextColor(getResources().getColor(R.color.colorDarkGreen));
                                    }
                                    pieView.setPercentage(((float) probT));
                                    animateTextView(0, ((int) probT), pieText);
                                    textComment.setText(comment);
                                    textAnalyze.setText(analysis);
                                    textAnalysisValueProbT.setText(String.valueOf(probT) + "%");
                                }

                                PieAngleAnimation animation = new PieAngleAnimation(pieView);
                                animation.setDuration(1800);
                                pieView.startAnimation(animation);
                            }
                        });


                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
                Utility.myProgressBar.hideProgressBar();
            }

            return isError;
        }
    }

    public void animateTextView(int initialValue, int finalValue, final TextView textview) {

        ValueAnimator valueAnimator = ValueAnimator.ofInt(initialValue, finalValue);
        valueAnimator.setDuration(1800);
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                textview.setText(valueAnimator.getAnimatedValue().toString() + "%");
            }
        });
        valueAnimator.start();
    }

    @Override
    public void onBackPressed() {

        Intent intent = new Intent(AnalyzeResultActivity.this, HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        finish();
        startActivity(intent);
        overridePendingTransition(R.anim.trans_left_in, R.anim.trans_left_out);
    }

}
