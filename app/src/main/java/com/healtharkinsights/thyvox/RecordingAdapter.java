package com.healtharkinsights.thyvox;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import com.healtharkinsights.thyvox.model.Recording;

/**
 * {@code }
 *
 * @author Romi Chandra
 * @version 1.0
 * @since 5/12/18
 */
public class RecordingAdapter extends RecyclerView.Adapter<RecordingAdapter.ViewHolder> {

    private ArrayList<Recording> recordingArrayList;
    private Context mContext;
    private MediaPlayer mPlayer;
    private boolean isPlaying = false, isSaved;
    private int last_index = -1, sampleID;
    private boolean isEditable = false;
    private ImageView imageView;

    public RecordingAdapter(Context context, ArrayList<Recording> recordingArrayList, boolean isSavedRecording, int sampleID) {
        this.mContext = context;
        this.recordingArrayList = recordingArrayList;
        this.isSaved = isSavedRecording;
        this.sampleID = sampleID;
    }

    public RecordingAdapter(Context context, ArrayList<Recording> recordingArrayList, boolean isSavedRecording, int sampleID,ImageView imageView) {
        this.mContext = context;
        this.recordingArrayList = recordingArrayList;
        this.isSaved = isSavedRecording;
        this.sampleID = sampleID;
        this.imageView=imageView;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.viewholder_recordings, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        setUpData(holder, position);
    }

    @Override
    public int getItemCount() {
        return recordingArrayList.size();
    }

    public void setEnabled(boolean value) {
        this.isEditable = value;
    }

    private void setUpData(ViewHolder holder, int position) {

        Recording recording = recordingArrayList.get(position);
        holder.textViewDuration.setText(String.valueOf(recording.getDuration() / 60) + "m " + String.valueOf(recording.getDuration() % 60) + "s");

        try {
            DateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.ENGLISH);
            DateFormat targetFormat = new SimpleDateFormat("Time hh:mm, MMMM dd, yyyy");
            Date date = originalFormat.parse(recording.getTime());
            String formattedDate = targetFormat.format(date);

            holder.textViewTime.setText(formattedDate);
        } catch (Exception e) {
        }

        if (recording.isPlaying()) {
            holder.imageViewPlay.setImageResource(R.drawable.ic_pause);
            holder.seekUpdation(holder);
        } else {
            holder.imageViewPlay.setImageResource(R.drawable.ic_play);
        }
        if (isEditable) {
            holder.imageViewDelete.setEnabled(true);
        } else {
            holder.imageViewDelete.setEnabled(false);
        }
        if (recording.getAnalysis() != null && !recording.getAnalysis().isEmpty() && !recording.getAnalysis().equals("null")) {
            //holder.textViewResultAnalyze.setText("Click here to analyze");
        } else {
            //`holder.textViewResultAnalyze.setText("Click here to analyze");
        }

        /*if (isSaved) {
            if (recording.isNew()) {
                //holder.textViewResultAnalyze.setVisibility(View.GONE);
                holder.imageViewAnalyze.setVisibility(View.GONE);
            } else {
                //holder.textViewResultAnalyze.setVisibility(View.VISIBLE);
                holder.imageViewAnalyze.setVisibility(View.VISIBLE);
            }

        } else {
            //holder.textViewResultAnalyze.setVisibility(View.GONE);
            holder.imageViewAnalyze.setVisibility(View.GONE);
        }*/

        holder.manageSeekBar(holder);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView imageViewPlay, imageViewDelete, imageViewAnalyze;
        SeekBar seekBar;
        ProgressBar progressAnalyze;
        TextView textViewDuration, textViewTime,  textViewResultDetailsAnalyze;
        private String recordingUri;
        private int lastProgress = 0;
        private Handler mHandler = new Handler();
        ViewHolder holder;

        public ViewHolder(View itemView) {
            super(itemView);

            imageViewPlay = itemView.findViewById(R.id.imageViewPlay);
            imageViewDelete = itemView.findViewById(R.id.imageViewDelete);
            imageViewAnalyze = itemView.findViewById(R.id.imageAnalyzeRecord);
            seekBar = itemView.findViewById(R.id.seekBar);
            textViewDuration = itemView.findViewById(R.id.textViewRecordingDuration);
            textViewTime = itemView.findViewById(R.id.textViewRecordingTime);
            //textViewResultAnalyze = itemView.findViewById(R.id.textResultAnalyzeRecord);
            textViewResultDetailsAnalyze = itemView.findViewById(R.id.textResultDetailsAnalyzeRecord);
            progressAnalyze = itemView.findViewById(R.id.analyzeRecordProgressBar);

           /* textViewResultAnalyze.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    progressAnalyze.setVisibility(View.VISIBLE);
                    imageViewAnalyze.setVisibility(View.GONE);
                    int position = getAdapterPosition();
                    Recording recording = recordingArrayList.get(position);
                    ApiManager.analyzeFile(recording.getFileName(), sampleID, new AnalyzeFileListener() {
                        @Override
                        public void onAnalysisSucces(String analysis, final AnalyzeResultModel model) {
                            ((AppCompatActivity) mContext).runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    progressAnalyze.setVisibility(View.GONE);
                                    imageViewAnalyze.setVisibility(View.VISIBLE);
                                    if (model.getComment().isEmpty()) {
                                    } else {
                                        Intent i = new Intent(mContext, AnalyzeResultActivity.class);
                                        i.putExtra("analysis", model.getAnalysis());
                                        i.putExtra("comment", model.getComment());
                                        i.putExtra("probT", model.getProbThyroid());
                                        i.putExtra("probN", model.getProbNormal());
                                        mContext.startActivity(i);
                                    }
                                }
                            });
                        }

                        @Override
                        public void onAnalysisError(final String error) {
                            ((AppCompatActivity) mContext).runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    progressAnalyze.setVisibility(View.GONE);
                                    imageViewAnalyze.setVisibility(View.VISIBLE);
                                    textViewResultAnalyze.setText("Click here to analyze");
                                    Toast.makeText(mContext, error, Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                    });
                }
            });*/

            textViewResultDetailsAnalyze.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(mContext, OldRecordActivity.class);
                    i.putExtra("sampleID",String.valueOf(recordingArrayList.get(getAdapterPosition()).getId()));
                    mContext.startActivity(i);

                }
            });

            imageViewPlay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = getAdapterPosition();
                    Recording recording = recordingArrayList.get(position);

                    if (isSaved) {
                        if (!recording.isNew()) {
                            recordingUri = recording.getFilePath();
                        } else {
                            recordingUri = recording.getFilePath() + recording.getFileName();
                        }
                    } else {
                        recordingUri = recording.getFilePath() + recording.getFileName();
                    }

                    if (isPlaying) {
                        stopPlaying();
                        if (position == last_index) {
                            recording.setPlaying(false);
                            stopPlaying();
                            notifyItemChanged(position);
                        } else {
                            markAllPaused();
                            recording.setPlaying(true);
                            notifyItemChanged(position);
                            startPlaying(recording, position);
                            last_index = position;
                        }

                    } else {
                        startPlaying(recording, position);
                        recording.setPlaying(true);
                        seekBar.setMax(mPlayer.getDuration());
                        Log.d("isPlaying", "False");
                        notifyItemChanged(position);
                        last_index = position;
                    }
                }
            });

            imageViewDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    final Dialog mDialog = new Dialog(mContext);
                    mDialog.setContentView(R.layout.custom_warn_dialoge);
                    mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                    Window dialogWindow = mDialog.getWindow();
                    dialogWindow.setGravity(Gravity.CENTER);
                    mDialog.show();

                    TextView tvMessage = mDialog.findViewById(R.id.tvMessage);
                    tvMessage.setText(R.string.sure_detete_recording);

                    Button btnYes = mDialog.findViewById(R.id.btn_yes);
                    Button btnCancel = mDialog.findViewById(R.id.btn_cancel);

                    btnCancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mDialog.dismiss();
                        }
                    });
                    btnYes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mDialog.dismiss();
                            final int position = getAdapterPosition();
                            Recording recording = recordingArrayList.get(position);
                            try {
                                File f = new File(recordingUri);
                                if (f.exists()) {
                                    f.delete();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            if (sampleID == -1) {
                                recordingArrayList.remove(position);
                                notifyItemRemoved(position);
                                if (imageView!=null&&recordingArrayList.size()==0){
                                    imageView.setVisibility(View.GONE);
                                }
                                return;
                            }
                           /* ApiManager.deleteFile(recording.getFileName(), sampleID, new DownloadListener() {
                                @Override
                                public void onDownloadSuccess(String response) {
                                    ((AppCompatActivity) mContext).runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            recordingArrayList.remove(position);
                                            notifyItemRemoved(position);
                                            Toast.makeText(mContext, "File deleted", Toast.LENGTH_SHORT).show();

                                        }
                                    });
                                }

                                @Override
                                public void onDownloadError(final String error) {
                                    ((AppCompatActivity) mContext).runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(mContext, "File not deleted, error : " + error, Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                }
                            });*/



                        }
                    });



                }
            });


        }




        public void manageSeekBar(ViewHolder holder) {
            holder.seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    if (mPlayer != null && fromUser) {
                        mPlayer.seekTo(progress);
                    }
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {

                }
            });
        }

        private void markAllPaused() {
            for (int i = 0; i < recordingArrayList.size(); i++) {
                recordingArrayList.get(i).setPlaying(false);
                recordingArrayList.set(i, recordingArrayList.get(i));
            }
            notifyDataSetChanged();
        }

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                seekUpdation(holder);
            }
        };

        private void seekUpdation(ViewHolder holder) {
            this.holder = holder;
            if (mPlayer != null) {
                int mCurrentPosition = mPlayer.getCurrentPosition();
                holder.seekBar.setMax(mPlayer.getDuration());
                holder.seekBar.setProgress(mCurrentPosition);
                lastProgress = mCurrentPosition;
            }
            mHandler.postDelayed(runnable, 100);
        }

        private void stopPlaying() {
            try {
                mPlayer.release();
            } catch (Exception e) {
                e.printStackTrace();
            }
            mPlayer = null;
            isPlaying = false;
        }

        private void startPlaying(final Recording audio, final int position) {
            mPlayer = new MediaPlayer();
            try {
                mPlayer.setDataSource(recordingUri);
                mPlayer.prepare();
                mPlayer.start();
            } catch (IOException e) {
                Log.e("LOG_TAG", "prepare() failed");
            }
            //showing the pause button
            seekBar.setMax(mPlayer.getDuration());
            isPlaying = true;

            mPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    audio.setPlaying(false);
                    notifyItemChanged(position);
                }
            });
        }

    }



}
