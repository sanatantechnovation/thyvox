package com.healtharkinsights.thyvox;

import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatSpinner;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import butterknife.ButterKnife;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import com.healtharkinsights.thyvox.helper.ApiCallFunction;
import com.healtharkinsights.thyvox.helper.AppSharedPreferences;
import com.healtharkinsights.thyvox.helper.Constant;
import com.healtharkinsights.thyvox.helper.Utility;
import com.healtharkinsights.thyvox.model.Recording;
import com.healtharkinsights.thyvox.model.RecordingInfoModel;
import com.healtharkinsights.thyvox.model.Sample;

public class OldRecordActivity extends AppCompatActivity {

    ImageButton mBackView;
    TextView mSaveView;
    TextView mTitleView;

    //private RecyclerView recyclerViewRecordings;

    EditText mNameView;
    AppCompatSpinner mDayView;
    AppCompatSpinner mMonthView;
    EditText mYearView;
    AppCompatSpinner mGenderView;
    EditText mWeightView;

    EditText mHeightFTView;
    EditText mHeightINView;
    AppCompatCheckBox mWeightGainView;
    AppCompatCheckBox mWeightLossView;
    AppCompatCheckBox mFatigueView;
    AppCompatCheckBox mColdView;
    AppCompatCheckBox mWeaknessView;
    AppCompatCheckBox mHairLossView;
    private String sampleID = "";
    private Button btnViewREsult;

    EditText mBloodPressureHighView;
    EditText mBloodPressureLowView;
    EditText mCholestrolView;
    EditText mT3View;
    EditText mT4View;
    EditText mTSHView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.WHITE);
        }
        setContentView(R.layout.activity_new_record);
        ButterKnife.bind(this);

        btnViewREsult = findViewById(R.id.btn_view_result);
        mHairLossView = findViewById(R.id.checkHairLossNewRecord);
        mWeaknessView = findViewById(R.id.checkWeaknessNewRecord);
        mColdView = findViewById(R.id.checkColdNewRecord);

        mFatigueView = findViewById(R.id.checkFatigueNewRecord);
        mWeightLossView = findViewById(R.id.checkRecentWeightLossNewRecord);
        mWeightGainView = findViewById(R.id.checkRecentWeightGainNewRecord);
        mHeightINView = findViewById(R.id.editTextHeightInchesNewRecord);
        mHeightFTView = findViewById(R.id.editTextHeightFtNewRecord);
        mWeightView = findViewById(R.id.editTextWeightNewRecord);
        mGenderView = findViewById(R.id.spinnerGenderNewRecord);
        mYearView = findViewById(R.id.editTextYearNewRecord);
        mMonthView = findViewById(R.id.spinnerMonthNewRecord);
        mDayView = findViewById(R.id.spinnerDateNewRecord);
        mNameView = findViewById(R.id.editTextRecordName);
        mTitleView = findViewById(R.id.toolBarTextSampleName);
        mSaveView = findViewById(R.id.toolBarTextSaveRecord);
        mBackView = findViewById(R.id.toolBarButtonBack);



       mBloodPressureHighView = findViewById(R.id.editTextBloodPressureNewRecord);
       mBloodPressureLowView = findViewById(R.id.editTextBloodPressureLowNewRecord);
       mCholestrolView = findViewById(R.id.editTextCholestrolNewRecord);
       mT3View = findViewById(R.id.editTextT3NewRecord);
       mT4View = findViewById(R.id.editTextT4NewRecord);
       mTSHView = findViewById(R.id.editTextTSHNewRecord);

        //recyclerViewRecordings = findViewById(R.id.recyclerViewRecordings);


        mSaveView.setVisibility(View.VISIBLE);
        mTitleView.setText("Sample #4");



        try {
            if (getIntent().hasExtra("sampleID")) {
                sampleID = getIntent().getStringExtra("sampleID");
                if (!sampleID.isEmpty()) {
                    new GetRecordingInfo().execute(sampleID);
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        mBackView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(OldRecordActivity.this, HomeActivity.class);
                finish();
                startActivity(i);
                overridePendingTransition(R.anim.trans_left_in, R.anim.trans_left_out);


            }
        });


        btnViewREsult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(OldRecordActivity.this, MyAnalyzeResultActivity.class);
                i.putExtra("InsertID", sampleID);
                i.putExtra("From","OldRecordActivity");
                finish();
                startActivity(i);
                overridePendingTransition(R.anim.trans_left_in, R.anim.trans_left_out);

            }
        });
    }


    public class GetRecordingInfo extends AsyncTask<String, String, String> {

        RecordingInfoModel recordingInfoModel;


        @Override
        protected void onPreExecute() {
            Utility.myProgressBar.showProgressBar(OldRecordActivity.this);
        }

        @Override
        protected void onPostExecute(String response) {
            Utility.myProgressBar.hideProgressBar();


            try {

                mNameView.setText(recordingInfoModel.getData().getSubject().getName());
                try {
                    Log.e( "onPostExecute: ", " DOB : "+recordingInfoModel.getData().getSubject().getDob());
                    String[] split = recordingInfoModel.getData().getSubject().getDob().split("-");
                    mDayView.setSelection(Integer.parseInt(split[2]) - 1, true);
                    mMonthView.setSelection(Integer.parseInt(split[1]) - 1, true);
                    mYearView.setText(String.valueOf(Integer.parseInt(split[0])));
                } catch (Exception e) {
                    e.printStackTrace();
                }

                mGenderView.setSelection(recordingInfoModel.getData().getSubject().getGender().equals("Female") ? 1 : 0);
                String[] splitHeight = recordingInfoModel.getData().getSubject().getHeight().split(" ");
                try {
                    mWeightView.setText(String.valueOf(recordingInfoModel.getData().getSubject().getWeight()));
                } catch (Exception e) {
                    e.printStackTrace();
                    mWeightView.setText("");
                }

                try {
                    mHeightFTView.setText(String.valueOf(Integer.parseInt(splitHeight[0].substring(0, splitHeight[0].length() - 2))));
                    mHeightINView.setText(String.valueOf(Integer.parseInt(splitHeight[1].substring(0, splitHeight[1].length() - 2))));
                } catch (Exception e) {
                    mHeightFTView.setText("");
                    mHeightINView.setText("");
                    e.printStackTrace();
                }

                mWeightGainView.setChecked(recordingInfoModel.getData().getSymptoms().getRWG().equals("1"));
                mWeightLossView.setChecked(recordingInfoModel.getData().getSymptoms().getRWL().equals("1"));
                mFatigueView.setChecked(recordingInfoModel.getData().getSymptoms().getTIR().equals("1"));
                mColdView.setChecked(recordingInfoModel.getData().getSymptoms().getCLD().equals("1"));
                mWeaknessView.setChecked(recordingInfoModel.getData().getSymptoms().getWEA().equals("1"));
                mHairLossView.setChecked(recordingInfoModel.getData().getSymptoms().getHAL().equals("1"));


                if (recordingInfoModel.getData().getSubject().getBpHigh()!=null)
                mBloodPressureHighView.setText(recordingInfoModel.getData().getSubject().getBpHigh());
                if (recordingInfoModel.getData().getSubject().getBpLow()!=null)
                mBloodPressureLowView.setText(recordingInfoModel.getData().getSubject().getBpLow());
                if (recordingInfoModel.getData().getSubject().getCholestrol()!=null)
                mCholestrolView.setText(recordingInfoModel.getData().getSubject().getCholestrol());
                if (recordingInfoModel.getData().getSubject().getT3()!=null)
                mT3View.setText(recordingInfoModel.getData().getSubject().getT3());
                if (recordingInfoModel.getData().getSubject().getT4()!=null)
                mT4View.setText(recordingInfoModel.getData().getSubject().getT4());
                if (recordingInfoModel.getData().getSubject().getTsh()!=null)
                mTSHView.setText(recordingInfoModel.getData().getSubject().getTsh());


            } catch (NullPointerException e) {
                e.printStackTrace();
            }


        }

        @Override
        protected String doInBackground(String... params) {

            String isError = "", response = "";
            response = ApiCallFunction.getRequest(Constant.BASE_URL +"samples/" + params[0]);
            if (response.length() > 0) {
                Log.e("GetRecordingInfo", " URL : "+Constant.BASE_URL +"samples/" + params[0]);
                Log.e("GetRecordingInfo", "Samples Details onResponse: "+response);
                recordingInfoModel = new Gson().fromJson(response, new TypeToken<RecordingInfoModel>() {}.getType());
                AppSharedPreferences.getInstance().setRecordingInfoModel(recordingInfoModel);


            }
            return isError;


        }
    }



    @Override
    public void onBackPressed() {
        Intent i = new Intent(this, HomeActivity.class);
        finish();
        startActivity(i);
        overridePendingTransition(R.anim.trans_left_in, R.anim.trans_left_out);
    }
}
