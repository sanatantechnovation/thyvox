package com.healtharkinsights.thyvox;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import com.healtharkinsights.thyvox.helper.State;
import com.healtharkinsights.thyvox.model.Recording;

public class HomeActivity extends BaseActivity implements View.OnClickListener {

    private RecyclerView mRecyclerViewRecordings;
    private ImageView mMicIcon, imgNext;
    private Chronometer mChronometer;
    private MediaRecorder mRecorder;
    private boolean isPlaying = false;
    private Recording recording;
    private MediaPlayer mPlayer;
    private RecordingAdapter recordingAdapter;
    private ArrayList<Recording> recordingArraylist = new ArrayList<>();

    private boolean doubleBackToExitPressedOnce = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.WHITE);
        }
        FrameLayout contentFrameLayout = findViewById(R.id.content_frame);
        getLayoutInflater().inflate(R.layout.activity_home, contentFrameLayout);

        mRecyclerViewRecordings = findViewById(R.id.recyclerViewRecordings);
        mChronometer = findViewById(R.id.chronometerTimerRecordings);
        ImageView imgMenu = findViewById(R.id.img_menu);
        imgMenu.setOnClickListener(this);
        mMicIcon = findViewById(R.id.attachRecordingIcon);
        mMicIcon.setOnClickListener(this);
        imgNext = findViewById(R.id.img_next);
        imgNext.setOnClickListener(this);

        initializeRecyclerView();
    }

    private void initializeRecyclerView() {
        initializeAdaptertoRecyclerView();
        mRecyclerViewRecordings.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
    }

    private void initializeAdaptertoRecyclerView() {
        recordingAdapter = new RecordingAdapter(this, recordingArraylist, false, -1, imgNext);
        recordingAdapter.setEnabled(true);
        mRecyclerViewRecordings.setAdapter(recordingAdapter);
        if (recordingArraylist.size() == 0) {
            imgNext.setVisibility(View.GONE);
        } else {
            imgNext.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.attachRecordingIcon: {
                if (isPlaying) {

                    int elapsedMillis = (int) (SystemClock.elapsedRealtime() - mChronometer.getBase());
                    int second = elapsedMillis / 1000;
                    Log.e("stopRecording: ", " Second : " + second);
                    if (second > 3) {
                        prepareforStop();
                        stopRecording();
                    } else {
                        Toast.makeText(this, "Require min 3 sec recording", Toast.LENGTH_SHORT).show();
                    }


                } else {
                    if (recordingArraylist.size() < 3) {
                        prepareforRecording();
                        startRecording();
                    } else {
                        AlertDialog("You can add only three recording");
                        return;
                    }

                }
                break;
            }
            case R.id.img_menu:
                openDrawer();
                break;

            case R.id.img_next:
                if (recordingArraylist.size() == 3) {
                    Intent myIntent = new Intent(HomeActivity.this, InfoDetailsActivity.class);
                    myIntent.putExtra("recordingListExtra", recordingArraylist);
                    finish();
                    startActivity(myIntent);
                    overridePendingTransition(R.anim.trans_left_in, R.anim.trans_left_out);
                } else {
                    AlertDialog("You must have three recording");
                    return;
                }


                break;

        }
    }

    private void prepareforRecording() {
        isPlaying = true;
        mChronometer.setVisibility(View.VISIBLE);
        mMicIcon.setBackground(getResources().getDrawable(R.drawable.red_circle_shadow));
        mMicIcon.setImageResource(0);
    }

    private void prepareforStop() {
        isPlaying = false;
        mChronometer.setVisibility(View.INVISIBLE);
        mMicIcon.setBackground(getResources().getDrawable(R.drawable.green_circle_shadow));
        mMicIcon.setImageResource(R.drawable.microphone);
    }


    private void stopRecording() {

        try {
            int elapsedMillis = (int) (SystemClock.elapsedRealtime() - mChronometer.getBase());
            int second = elapsedMillis / 1000;
            mRecorder.stop();
            mRecorder.release();
            recording.setDuration(second);

        } catch (Exception e) {
            e.printStackTrace();
        }
        mRecorder = null;
        mChronometer.stop();
        mChronometer.setBase(SystemClock.elapsedRealtime());
        mRecyclerViewRecordings.setVisibility(View.VISIBLE);
        recording.setTime(getCurrentDateTime());
        recording.setNew(true);
        addRecording(recording);

    }

    private void startRecording() {

        recordingAdapter.notifyDataSetChanged();
        mRecorder = new MediaRecorder();
        mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        File root = android.os.Environment.getExternalStorageDirectory();
        String fileName = String.valueOf(System.currentTimeMillis());
        String filePath = root.getAbsolutePath() + State.PATH_NIRVANA;
        File file = new File(filePath);
        if (!file.exists()) {
            file.mkdirs();
        }

        Log.d("strFileName", filePath);
        mRecorder.setOutputFile(filePath + fileName);
        mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);


        try {
            mRecorder.prepare();
            mRecorder.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
        stopPlaying();
        mChronometer.setBase(SystemClock.elapsedRealtime());
        mChronometer.start();
        recording = new Recording();
        recording.setFileName(fileName);
        recording.setFilePath(filePath);
        recording.setTimestamp(new Date());
    }

    private void stopPlaying() {
        try {
            mPlayer.release();
        } catch (Exception e) {
            e.printStackTrace();
        }
        mPlayer = null;
        mChronometer.stop();
        recording = null;
    }

    private void addRecording(Recording recording) {
        if (recordingAdapter != null) {
            recordingArraylist.add(recording);
            recordingAdapter.notifyItemInserted(recordingArraylist.size());
            if (recordingArraylist.size() == 0) {
                imgNext.setVisibility(View.GONE);
            } else {
                imgNext.setVisibility(View.VISIBLE);
            }

        }
    }

    private String getCurrentDateTime() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return df.format(c.getTime());
    }


    /*private void clearDirectory(){
        try {
            File root = android.os.Environment.getExternalStorageDirectory();
            String path = root.getAbsolutePath() + State.PATH_NIRVANA;
            FilesManager.getInstance(HomeActivity.this).clearDirectory(path);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/


    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }
        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Press again to exit Thyvox", Toast.LENGTH_SHORT).show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }


    public void AlertDialog(String message) {
        final Dialog mDialog = new Dialog(this);
        mDialog.setContentView(R.layout.custom_alert_dialoge);
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        Window dialogWindow = mDialog.getWindow();
        dialogWindow.setGravity(Gravity.CENTER);
        mDialog.show();

        TextView tvMessage = mDialog.findViewById(R.id.tvMessage);
        tvMessage.setText(message);

        Button btnOK = mDialog.findViewById(R.id.btn_ok);

        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.cancel();


            }
        });


    }
}
