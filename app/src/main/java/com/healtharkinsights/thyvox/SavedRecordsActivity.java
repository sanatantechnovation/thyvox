package com.healtharkinsights.thyvox;

import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.healtharkinsights.thyvox.helper.AppSharedPreferences;
import com.healtharkinsights.thyvox.helper.Constant;
import com.healtharkinsights.thyvox.helper.ApiCallFunction;
import com.healtharkinsights.thyvox.helper.Utility;
import com.healtharkinsights.thyvox.interfaces.DeleteSampleInterface;
import com.healtharkinsights.thyvox.model.DeleteSampleModel;
import com.healtharkinsights.thyvox.model.SampleListModel;

public class SavedRecordsActivity extends BaseActivity implements DeleteSampleInterface {

    @BindView(R.id.recyclerViewSavedRecordings)
    RecyclerView recyclerViewRecordings;
    @BindView(R.id.toolBarButtonBack)
    ImageButton buttonBack;

    private SavedRecordsAdapter savedRecordsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.WHITE);
        }
        FrameLayout contentFrameLayout = findViewById(R.id.content_frame);
        getLayoutInflater().inflate(R.layout.activity_saved_records, contentFrameLayout);

        ButterKnife.bind(this);
        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SavedRecordsActivity.this, HomeActivity.class);
                finish();
                startActivity(intent);
                overridePendingTransition(R.anim.trans_left_in, R.anim.trans_left_out);
            }
        });


        getSampleAPI();


    }


    private void getSampleAPI(){
        if (AppSharedPreferences.getInstance().getUserType() == 1) {
            new getSample().execute("user_samples", AppSharedPreferences.getInstance().getUserId());
        } else {
            new getSample().execute("adminSamples");

        }
    }


    @Override
    public void onBackPressed() {
        Intent intent = new Intent(SavedRecordsActivity.this, HomeActivity.class);
        finish();
        startActivity(intent);
        overridePendingTransition(R.anim.trans_left_in, R.anim.trans_left_out);
    }

    @Override
    public void deleteSampleCall(int position, String sampleId) {
        new DeleteSample().execute(sampleId);

    }


    public class getSample extends AsyncTask<String, String, String> {
        SampleListModel subjectSamples = null;

        @Override
        protected void onPreExecute() {
            Utility.myProgressBar.showProgressBar(SavedRecordsActivity.this);
        }

        @Override
        protected void onPostExecute(String response) {
            Utility.myProgressBar.hideProgressBar();

            if (subjectSamples!=null) {
                Log.e("onPostExecute: ", "subjectSamples Not Null");
                List<SampleListModel.Datum> data = subjectSamples.getData();
                LinearLayoutManager llm = new LinearLayoutManager(SavedRecordsActivity.this);
                llm.setOrientation(LinearLayoutManager.VERTICAL);
                recyclerViewRecordings.setLayoutManager(llm);
                savedRecordsAdapter = new SavedRecordsAdapter(data, SavedRecordsActivity.this);
                recyclerViewRecordings.setAdapter(savedRecordsAdapter);
                savedRecordsAdapter.notifyDataSetChanged();
            }else {
                recyclerViewRecordings.removeAllViewsInLayout();
            }
        }

        @Override
        protected String doInBackground(String... params) {
            String isError = "", response = "";
            String strUrl = "";
            if (params[0].contains("user_samples")) {
                strUrl = Constant.BASE_URL + params[0] + "/" + params[1];
            } else {
                strUrl = Constant.BASE_URL + params[0];
            }

            Log.e( "user_samples: "," URL : "+strUrl );
            response = ApiCallFunction.getRequest(strUrl);
            if (response.length() > 0) {
                Log.e( "user_samples: "," response : "+response);
                subjectSamples = new Gson().fromJson(response, new TypeToken<SampleListModel>() {}.getType());

            }
            return isError;
        }
    }


    public class DeleteSample extends AsyncTask<String, String, String> {
        DeleteSampleModel deleteSampleModel;

        @Override
        protected void onPreExecute() {
            Utility.myProgressBar.showProgressBar(SavedRecordsActivity.this);
        }

        @Override
        protected void onPostExecute(String response) {
            Utility.myProgressBar.hideProgressBar();
            if (deleteSampleModel != null) {
                if (deleteSampleModel.getIsError().equals(false)) {
                    getSampleAPI();
                }
            }
        }

        @Override
        protected String doInBackground(String... params) {
            HashMap<String, String> parameters = new HashMap<>();
            parameters.put("sample_id", params[0]);
            String isError = "", response = "";

            Log.e( "delete_sample: "," parameters : "+parameters.toString());


            response = ApiCallFunction.postRequest(SavedRecordsActivity.this, Constant.BASE_URL + "/delete_sample", parameters);
            if (response.length() > 0) {
                Log.e( "delete_sample: "," response : "+response);
                try{
                    deleteSampleModel = new Gson().fromJson(response, new TypeToken<DeleteSampleModel>() {}.getType());
                }catch (IllegalStateException e){
                    e.printStackTrace();
                }

            }
            return isError;
        }
    }

}
