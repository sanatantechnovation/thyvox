package com.healtharkinsights.thyvox;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;
import com.healtharkinsights.thyvox.helper.FilesManager;

/**
 * {@code }
 *
 * @author Romi Chandra
 * @version 1.0
 * @since 5/12/18
 */
public class ThyvoxApp extends Application {

    public static final String TAG = ThyvoxApp.class.getSimpleName();
    private static ThyvoxApp mInstance;
    private static Context mContext;

    @Override
    public void onCreate() {
        super.onCreate();
        FilesManager.getInstance(this).start();
        mContext = getApplicationContext();
        mInstance = this;
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        FilesManager.getInstance(this).stop();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public Context getApplicationContext() {
        return super.getApplicationContext();
    }

    public static synchronized ThyvoxApp getInstance() {
        if (mInstance == null) {
            mInstance = new ThyvoxApp();
        }
        return mInstance;
    }
}
