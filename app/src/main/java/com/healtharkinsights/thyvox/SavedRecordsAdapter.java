package com.healtharkinsights.thyvox;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import com.healtharkinsights.thyvox.helper.Utility;
import com.healtharkinsights.thyvox.interfaces.DeleteSampleInterface;
import com.healtharkinsights.thyvox.model.SampleListModel;

/**
 * {@code }
 *
 * @author Presh Patel
 * @version 1.0
 * @since 21/1/19
 */
public class SavedRecordsAdapter extends RecyclerView.Adapter<SavedRecordsAdapter.ViewHolder> {

    private List<SampleListModel.Datum> sampleArrayList;
    Context mContext;
    DeleteSampleInterface viewItemInterface;


    public SavedRecordsAdapter(List<SampleListModel.Datum> sampleArrayList, Context context) {
        this.sampleArrayList = sampleArrayList;
        this.mContext = context;
        this.viewItemInterface = (DeleteSampleInterface) context;

    }

    @NonNull
    @Override
    public SavedRecordsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.viewholder_saved_sample_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final SavedRecordsAdapter.ViewHolder holder, final int position) {

        try {
            holder.textViewName.setText(String.valueOf(position + 1)+". "+Utility.getCaps(sampleArrayList.get(position).getName()));
            DateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.ENGLISH);
            DateFormat targetFormat = new SimpleDateFormat("dd MMM yyyy, hh:mm a");
            Date date = originalFormat.parse(sampleArrayList.get(position).getDateAdded());
            String formattedDate = targetFormat.format(date);
            holder.textViewTime.setText(formattedDate);
        } catch (Exception e) {
        }

        holder.mainContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!String.valueOf(sampleArrayList.get(position).getId()).isEmpty()){
                    Activity activity = (Activity) mContext;
                    Intent intent = new Intent(mContext,OldRecordActivity.class);
                    intent.putExtra("sampleID",String.valueOf(sampleArrayList.get(position).getId()));
                    activity.finish();
                    activity.startActivity(intent);
                    activity.overridePendingTransition(R.anim.trans_left_in, R.anim.trans_left_out);
                }

            }
        });
        holder.imageDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog mDialog = new Dialog(mContext);
                mDialog.setContentView(R.layout.custom_warn_dialoge);
                mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                Window dialogWindow = mDialog.getWindow();
                dialogWindow.setGravity(Gravity.CENTER);
                mDialog.show();

                TextView tvMessage = mDialog.findViewById(R.id.tvMessage);
                tvMessage.setText(R.string.sure_delete_file);

                Button btnYes = mDialog.findViewById(R.id.btn_yes);
                Button btnCancel = mDialog.findViewById(R.id.btn_cancel);

                btnCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mDialog.dismiss();
                    }
                });
                btnYes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mDialog.dismiss();
                        if (viewItemInterface != null) {
                            viewItemInterface.deleteSampleCall(holder.getAdapterPosition(),sampleArrayList.get(position).getId());
                        }



                    }
                });


            }
        });
    }

    @Override
    public void onBindViewHolder(@NonNull SavedRecordsAdapter.ViewHolder holder, int position, @NonNull List<Object> payloads) {
        super.onBindViewHolder(holder, position, payloads);
    }

    @Override
    public int getItemCount() {
        return sampleArrayList.size();
    }


    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }



    public class ViewHolder extends RecyclerView.ViewHolder {
        View mainContainer;
        TextView  textViewName, textViewTime;
        ImageView imageDelete;

        public ViewHolder(View itemView) {
            super(itemView);

            mainContainer = itemView.findViewById(R.id.mainContainerViewHolderSavedSample);
            textViewName = itemView.findViewById(R.id.textSavedRecordName);
            textViewTime = itemView.findViewById(R.id.textSavedRecordTime);
            imageDelete = itemView.findViewById(R.id.imageDeleteSample);
        }
    }



}
