package com.healtharkinsights.thyvox.helper;

import android.content.Context;
import android.content.SharedPreferences;

import com.healtharkinsights.thyvox.ThyvoxApp;
import com.healtharkinsights.thyvox.model.RecordingInfoModel;
import com.healtharkinsights.thyvox.model.UploadAudioModel;

public class AppSharedPreferences {

    private static AppSharedPreferences instance;
    private String TAG = this.getClass().getSimpleName();
    private SharedPreferences mPrefs;
    private SharedPreferences.Editor mPrefsEditor;

    private final String TOKEN_VALUE = "token_value";
    private final String USER_ID = "user_id";
    private final String USER_NAME = "user_name";
    private final String USER_EMAIL = "user_email";
    private final String IS_USER_LOGIN  = "isUserLoggedIn";

    private final String USER_TYPE  = "usertype";


    private final String FIRST_ANALYZE_RESULT  = "firstanalyzeresult";
    private final String SECOND_ANALYZE_RESULT  = "secondanalyzeresult";
    private final String THIRD_ANALYZE_RESULT  = "thirdanalyzeresult";



    private final String FIRST_PROB_THYROID  = "first_prob_thyroid";
    private final String SECOND_PROB_THYROID  = "second_prob_thyroid";
    private final String THIRD_PROB_THYROID  = "third_prob_thyroid";


    private final String UPLOAD_AUDIO_RESPONSE  = "upload_audio_response";
    private final String RECORDING_INFO_MODEL  = "recordingInfoModel";




    public AppSharedPreferences(Context context) {

        this.mPrefs = context.getSharedPreferences(TAG, Context.MODE_PRIVATE);
        this.mPrefsEditor = mPrefs.edit();
    }

    public static AppSharedPreferences getInstance() {
        if (instance == null) {
            instance = new AppSharedPreferences(ThyvoxApp.getInstance().getApplicationContext());
        }
        return instance;
    }

    public void clearPreference(Context context) {
        setTokenValue("");
        SharedPreferences.Editor editor = this.mPrefs.edit();
        editor.clear();
        editor.commit();
    }



    public String getTokenValue() {
        return mPrefs.getString(TOKEN_VALUE, "");
    }

    public void setTokenValue(String token_value) {
        mPrefsEditor.putString(TOKEN_VALUE, token_value);
        mPrefsEditor.commit();
    }




    public int getUserLogin() {
        return mPrefs.getInt(IS_USER_LOGIN, 0);
    }

    public void setUserLogin(int value) {
        mPrefsEditor.putInt(IS_USER_LOGIN, value);
        mPrefsEditor.commit();
    }




    public String getUserId() {
        return mPrefs.getString(USER_ID, "");
    }

    public void setUserId(String value) {
        mPrefsEditor.putString(USER_ID, value);
        mPrefsEditor.commit();
    }


    public String getUserName() {
        return mPrefs.getString(USER_NAME, "");
    }

    public void setUserName(String value) {
        mPrefsEditor.putString(USER_NAME, value);
        mPrefsEditor.commit();
    }


    public String getUserMail() {
        return mPrefs.getString(USER_EMAIL, "");
    }

    public void setUserMail(String value) {
        mPrefsEditor.putString(USER_EMAIL, value);
        mPrefsEditor.commit();
    }


    public int getUserType() {
        return mPrefs.getInt(USER_TYPE, 1);
    }

    public void setUserType(int value) {
        mPrefsEditor.putInt(USER_TYPE, value);
        mPrefsEditor.commit();
    }




    public String getFirstAnalyze() {
        return mPrefs.getString(FIRST_ANALYZE_RESULT, "");
    }

    public void setFirstAnalyze(String value) {
        mPrefsEditor.putString(FIRST_ANALYZE_RESULT, value);
        mPrefsEditor.commit();
    }

    public String getSecondAnalyze() {
        return mPrefs.getString(SECOND_ANALYZE_RESULT, "");
    }

    public void setSecondAnalyze(String value) {
        mPrefsEditor.putString(SECOND_ANALYZE_RESULT, value);
        mPrefsEditor.commit();
    }

    public String getThirdAnalyze() {
        return mPrefs.getString(SECOND_ANALYZE_RESULT, "");
    }

    public void setThirdAnalyze(String value) {
        mPrefsEditor.putString(THIRD_ANALYZE_RESULT, value);
        mPrefsEditor.commit();
    }



    public float getFirstProThyropid() {
        return mPrefs.getFloat(FIRST_PROB_THYROID, 0);
    }

    public void setFirstProThyropid(float value) {
        mPrefsEditor.putFloat(FIRST_PROB_THYROID, value);
        mPrefsEditor.commit();
    }

    public float getSecondProThyropid() {
        return mPrefs.getFloat(SECOND_PROB_THYROID, 0);
    }

    public void setSecondProThyropid(float value) {
        mPrefsEditor.putFloat(SECOND_PROB_THYROID, value);
        mPrefsEditor.commit();
    }


    public float getThirdProThyropid() {
        return mPrefs.getFloat(THIRD_PROB_THYROID, 0);
    }

    public void setThirdProThyropid(float value) {
        mPrefsEditor.putFloat(THIRD_PROB_THYROID, value);
        mPrefsEditor.commit();
    }


    public UploadAudioModel getUploadAudioModel() {
        String mPrefsString = mPrefs.getString(UPLOAD_AUDIO_RESPONSE, "");
        UploadAudioModel audioModel = Utility.JsonUtils.fromJson(mPrefsString, UploadAudioModel.class);
        return audioModel;
    }

    public void setUploadAudioModel(UploadAudioModel monitorLoginSetting) {
        String save_sate = Utility.JsonUtils.toJson(monitorLoginSetting);
        mPrefsEditor.putString(UPLOAD_AUDIO_RESPONSE, save_sate);
        mPrefsEditor.commit();
    }


    public RecordingInfoModel getRecordingInfoModel() {
        String mPrefsString = mPrefs.getString(RECORDING_INFO_MODEL, "");
        RecordingInfoModel audioModel = Utility.JsonUtils.fromJson(mPrefsString, RecordingInfoModel.class);
        return audioModel;
    }

    public void setRecordingInfoModel(RecordingInfoModel monitorLoginSetting) {
        String save_sate = Utility.JsonUtils.toJson(monitorLoginSetting);
        mPrefsEditor.putString(RECORDING_INFO_MODEL, save_sate);
        mPrefsEditor.commit();
    }





}
