package com.healtharkinsights.thyvox.helper;

import android.app.Activity;
import android.app.ProgressDialog;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utility {

    public static final class myProgressBar {
        public static ProgressDialog ProgressBar;

        public static void showProgressBar(Activity parent) {
            final ProgressDialog progressDialog = new ProgressDialog(parent);
            progressDialog.setMessage("Please wait..");
            progressDialog.setCancelable(false);
            ProgressBar = progressDialog;
            ProgressBar.show();
        }

        public static void hideProgressBar() {
            if (ProgressBar!=null && ProgressBar.isShowing()) ProgressBar.dismiss();
        }
    }

    public static boolean isEditTextContainEmail(String argEditText) {

        try {
            Pattern pattern = Pattern.compile("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
            Matcher matcher = pattern.matcher(argEditText);
            return matcher.matches();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }


    public static String getCaps(String tagName) {
        String[] splits = tagName.toLowerCase().split(" ");
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < splits.length; i++) {
            String eachWord = splits[i];
            if (i > 0 && eachWord.length() > 0) {
                sb.append(" ");
            }
            String cap = eachWord.substring(0, 1).toUpperCase()
                    + eachWord.substring(1);
            sb.append(cap);
        }
        return sb.toString();
    }

    public static class JsonUtils {

        public static String toJson(Object object) {

            Gson gson = new GsonBuilder().create();
            return gson.toJson(object);
        }

        public static <T> T fromJson(String jsonString, Class<T> classType) {

            Gson gson = new GsonBuilder().create();
            return gson.fromJson(jsonString, classType);
        }

    }
}
