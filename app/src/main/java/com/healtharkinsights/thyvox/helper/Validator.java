package com.healtharkinsights.thyvox.helper;

/**
 * {@code }
 *
 * @author Romi Chandra
 * @version 1.0
 * @since 6/15/18
 */
public class Validator {

    public static final float INVALID_SAMPLE_FIELD_FLOAT_VALUE = 111111.00f;
    public static final int INVALID_SAMPLE_FIELD_INT_VALUE = 1178;
}
