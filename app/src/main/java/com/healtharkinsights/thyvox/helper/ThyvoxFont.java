package com.healtharkinsights.thyvox.helper;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class ThyvoxFont extends TextView {

    public ThyvoxFont(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public ThyvoxFont(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ThyvoxFont(Context context) {
        super(context);
        init();
    }

    public void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "font/cloud_calligraphy.ttf");
        setTypeface(tf ,1);
    }
}
