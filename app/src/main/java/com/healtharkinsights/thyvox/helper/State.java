package com.healtharkinsights.thyvox.helper;

import java.util.ArrayList;

import com.healtharkinsights.thyvox.model.Sample;

/**
 * {@code }
 *
 * @author Romi Chandra
 * @version 1.0
 * @since 5/8/18
 */
public class State {

    public static Sample sample;
    public static ArrayList<Sample> samples = new ArrayList<>();

    public static final int OPEN_FOR_SAVED_RECORD = 1;
    public static final int OPEN_FOR_NEW_RECORD = 0;
    public static final String KEY_OPEN_FOR_SAVED_RECORD = "OPEN_FOR_SAVED_RECORD";
    public static final String KEY_OPEN_FOR_SAVED_RECORD_POSITION = "OPEN_FOR_SAVED_RECORD_POSITION";

    public static final String PATH_ORIGIN = "/Verbe/";
    public static final String PATH_NIRVANA = "/Verbe/nirvana/";
    public static final String PATH_DUMP = "/Verbe/dump/";
}
