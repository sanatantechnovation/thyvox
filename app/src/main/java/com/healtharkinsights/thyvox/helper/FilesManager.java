package com.healtharkinsights.thyvox.helper;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;

import java.io.File;

/**
 * {@code }
 *
 * @author Romi Chandra
 * @version 1.0
 * @since 5/12/18
 */
public class FilesManager {

    private final int MSG_SAVE_FILE_TO_PATH = 1;
    private final int MSG_MOVE_FILE_TO_PATH = 2;
    private final int MSG_CLEAR_DIRECTORY = 3;

    private final String BUNDLE_KEY_CLEAR_DIRECTORY = "CLEAR_DIRECTORY";
    private final String BUNDLE_KEY_SAVE_FILE_TO_PATH = "SAVE_FILE_TO_PATH";
    private final String BUNDLE_KEY_MOVE_FILE_TO_PATH = "MSG_MOVE_FILE_TO_PATH";
    private final String BUNDLE_KEY_MOVE_FILE_FROM_PATH = "MOVE_FILE_FROM_PATH";

    private static FilesManager filesManager;
    private FileOperationsHandlerThread fileOperationsHandlerThread;

    private FilesManager(Context context) {
        this.fileOperationsHandlerThread = new FileOperationsHandlerThread("FileOperationsHandlerThread", context);
    }

    public static FilesManager getInstance(Context context) {
        if (filesManager == null) {
            filesManager = new FilesManager(context);
        }
        return filesManager;
    }

    public void start() {
        fileOperationsHandlerThread.start();
    }

    public void stop() { fileOperationsHandlerThread.quit(); }

    public void save (String absolutePath) throws Exception {
        if (fileOperationsHandlerThread.isAlive()) {
            Bundle data = new Bundle();
            data.putString(BUNDLE_KEY_SAVE_FILE_TO_PATH, absolutePath);
            Message msg = new Message();
            msg.what = MSG_SAVE_FILE_TO_PATH;
            msg.setData(data);
            fileOperationsHandlerThread.postMessage(msg);
        } else {
            throw new Exception("FilesManager is not alive");
        }
    }

    public void move (String oldPath, String newPath) throws Exception {
        if (fileOperationsHandlerThread.isAlive()) {
            Bundle data = new Bundle();
            data.putString(BUNDLE_KEY_MOVE_FILE_TO_PATH, oldPath);
            data.putString(BUNDLE_KEY_MOVE_FILE_FROM_PATH, newPath);
            Message msg = new Message();
            msg.what = MSG_MOVE_FILE_TO_PATH;
            msg.setData(data);
            fileOperationsHandlerThread.postMessage(msg);
        } else {
            throw new Exception("FilesManager is not alive");
        }
    }

    public void clearDirectory(String absolutePath) throws Exception{
        if (fileOperationsHandlerThread.isAlive()) {
            Bundle data = new Bundle();
            data.putString(BUNDLE_KEY_CLEAR_DIRECTORY, absolutePath);
            Message msg = new Message();
            msg.what = MSG_CLEAR_DIRECTORY;
            msg.setData(data);
            fileOperationsHandlerThread.postMessage(msg);
        } else {
            throw new Exception("FilesManager is not alive");
        }
    }

    private class FileOperationsHandlerThread extends HandlerThread {
        private Handler mHandler;
        Context context;

        public FileOperationsHandlerThread(String name, Context context) {
            super(name);
            this.context = context;
        }

        @Override
        protected void onLooperPrepared() {
            mHandler = new Handler(Looper.myLooper()) {
                @Override
                public void handleMessage(Message msg) {
                    super.handleMessage(msg);
                    switch (msg.what) {
                        case MSG_SAVE_FILE_TO_PATH: {
                            FileCOO.saveFileToPath(msg.getData().getString(BUNDLE_KEY_SAVE_FILE_TO_PATH), context);
                            break;
                        }

                        case MSG_MOVE_FILE_TO_PATH: {
                            FileCOO.move(context,
                                    msg.getData().getString(BUNDLE_KEY_MOVE_FILE_TO_PATH),
                                    msg.getData().getString(BUNDLE_KEY_MOVE_FILE_FROM_PATH)
                            );
                            break;
                        }

                        case MSG_CLEAR_DIRECTORY: {
                            FileCOO.clearDirectory(msg.getData().getString(BUNDLE_KEY_CLEAR_DIRECTORY));
                            break;
                        }
                    }
                }
            };
        }

        public void postMessage(Message msg) {
            if (msg != null)
                mHandler.sendMessage(msg);
        }
    }

    private static class FileCOO {

        public static void saveFileToPath(String absolutePath, Context context) {

        }

        public static void move(Context context, String oldPath, String newPath) {
            try {
                File file = new File(oldPath);
                if (file.exists()) {
                    file.renameTo(new File(newPath));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public static void clearDirectory(String absolutePath) {
            try {
                File dir = new File(absolutePath);
                dir.renameTo(new File(State.PATH_ORIGIN + File.separator + State.PATH_DUMP + String.valueOf(System.currentTimeMillis())));
                deleteRecursive(dir);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        static void deleteRecursive(File fileOrDirectory) {
            if (fileOrDirectory.isDirectory()) {
                for (File child : fileOrDirectory.listFiles()) {
                    deleteRecursive(child);
                }
            }
            fileOrDirectory.delete();
        }

    }


}


