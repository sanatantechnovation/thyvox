package com.healtharkinsights.thyvox;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import com.healtharkinsights.thyvox.helper.AppSharedPreferences;
import com.healtharkinsights.thyvox.helper.Constant;
import com.healtharkinsights.thyvox.helper.ApiCallFunction;
import com.healtharkinsights.thyvox.helper.Utility;
import com.healtharkinsights.thyvox.model.LoginModel;

import java.util.HashMap;


public class LoginActivity extends AppCompatActivity {

    private int RECORD_AUDIO_REQUEST_CODE = 123;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.WHITE);
        }
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_login);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getPermissionToRecordAudio();
        }

        final EditText etEmail = findViewById(R.id.input_email);
        final EditText edPassword = findViewById(R.id.input_password);

        findViewById(R.id.btn_login).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isAllDone = true;
                String email = etEmail.getText().toString();
                String password = edPassword.getText().toString();

                boolean validEmail = Utility.isEditTextContainEmail(email);

                if (!validEmail) {
                    etEmail.setError("Enter valid email id");
                    isAllDone = false;
                }

                if (password.isEmpty()) {
                    edPassword.setError("Enter Password");
                    isAllDone = false;
                }

                if (isAllDone) {
                    new Login().execute(etEmail.getText().toString(), edPassword.getText().toString());
                }


            }
        });

        findViewById(R.id.link_signup).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, SignupActivity.class);
                finish();
                startActivity(intent);
                overridePendingTransition(R.anim.trans_left_in, R.anim.trans_left_out);
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void getPermissionToRecordAudio() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.RECORD_AUDIO, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    RECORD_AUDIO_REQUEST_CODE);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        if (requestCode == RECORD_AUDIO_REQUEST_CODE) {
            if (grantResults.length == 3 &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED
                    && grantResults[2] == PackageManager.PERMISSION_GRANTED) {
            } else {
                Toast.makeText(this, "You must give permissions to use this app. App is exiting.", Toast.LENGTH_SHORT).show();
                finishAffinity();
            }
        }
    }


    public class Login extends AsyncTask<String, String, String> {

        LoginModel mLoginResponse;

        @Override
        protected void onPreExecute() {
            Utility.myProgressBar.showProgressBar(LoginActivity.this);
        }

        @Override
        protected void onPostExecute(String response) {
            Utility.myProgressBar.hideProgressBar();
            if (mLoginResponse.getIsError().equals(false)) {
                Intent intent = new Intent(LoginActivity.this, AboutActivity.class);
                intent.putExtra("showButton", true);
                finish();
                startActivity(intent);
                overridePendingTransition(R.anim.trans_left_in, R.anim.trans_left_out);
            } else {
                Toast.makeText(LoginActivity.this, mLoginResponse.getMessage(), Toast.LENGTH_SHORT).show();
            }


        }

        @Override
        protected String doInBackground(String... params) {

            HashMap<String, String> parameters = new HashMap<>();
            parameters.put("email", params[0]);
            parameters.put("password", params[1]);
            Log.e("LoginActivity ", "login parameters : " + parameters);

            String isError = "", response = "";
            response = ApiCallFunction.postRequest(LoginActivity.this, Constant.BASE_URL + "login_user", parameters);
            if (response.length() > 0) {
                Log.e("LoginActivity ", "login response : " + response);
                mLoginResponse = new Gson().fromJson(response, new TypeToken<LoginModel>() {
                }.getType());
                if (mLoginResponse.getIsError().equals(false)) {

                    AppSharedPreferences.getInstance().setUserLogin(1);
                    Log.e("LoginActivity ", "login_id : " + String.valueOf(mLoginResponse.getData().getUserDetails().getId()));
                    Log.e("LoginActivity ", "token : " + String.valueOf(mLoginResponse.getData().getSession().getToken()));

                    if (mLoginResponse.getData().getUserDetails().getId() != null)
                        AppSharedPreferences.getInstance().setUserId(String.valueOf(mLoginResponse.getData().getUserDetails().getId()));
                    if (mLoginResponse.getData().getSession().getToken() != null)
                        AppSharedPreferences.getInstance().setTokenValue(mLoginResponse.getData().getSession().getToken());
                    if (mLoginResponse.getData().getUserDetails().getEmail() != null)
                        AppSharedPreferences.getInstance().setUserMail(mLoginResponse.getData().getUserDetails().getEmail());
                    if (mLoginResponse.getData().getUserDetails().getName() != null)
                        AppSharedPreferences.getInstance().setUserName(mLoginResponse.getData().getUserDetails().getName());
                    if (mLoginResponse.getData().getUserDetails().getUserType() != null)
                        AppSharedPreferences.getInstance().setUserType(Integer.parseInt(mLoginResponse.getData().getUserDetails().getUserType()));


                }


            }
            return isError;
        }
    }


}
