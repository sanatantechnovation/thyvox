package com.healtharkinsights.thyvox;

import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatSpinner;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import com.healtharkinsights.thyvox.helper.AppSharedPreferences;
import com.healtharkinsights.thyvox.helper.Constant;
import com.healtharkinsights.thyvox.helper.ApiCallFunction;
import com.healtharkinsights.thyvox.helper.State;
import com.healtharkinsights.thyvox.helper.Utility;
import com.healtharkinsights.thyvox.helper.Validator;
import com.healtharkinsights.thyvox.model.ApiResponseModel.SaveSampleModel;
import com.healtharkinsights.thyvox.model.Recording;
import com.healtharkinsights.thyvox.model.Sample;
import com.healtharkinsights.thyvox.model.UploadAudioModel;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class InfoDetailsActivity extends AppCompatActivity implements View.OnClickListener {

    private ArrayList<Recording> recordingArraylist = new ArrayList<>();

    private EditText mNameView, mYearView, mWeightView, mHeightFTView, mHeightINView, mBloodPressureHighView, mBloodPressureLowView, mCholestrolView, mT3View, mT4View, mTSHView;
    private AppCompatSpinner mMonthView, mDayView, mGenderView;
    private AppCompatCheckBox mWeightGainView, mWeightLossView, mFatigueView, mColdView, mWeaknessView, mHairLossView;


    //DOB, Name, and Gender

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.WHITE);
        }
        setContentView(R.layout.activity_info_details);

        if (getIntent().hasExtra("recordingListExtra")) {
            recordingArraylist = (ArrayList<Recording>) getIntent().getSerializableExtra("recordingListExtra");
        }

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        init();
    }


    public void init() {
        mNameView = findViewById(R.id.editTextRecordName);
        mYearView = findViewById(R.id.editTextYearNewRecord);
        mMonthView = findViewById(R.id.spinnerMonthNewRecord);
        mDayView = findViewById(R.id.spinnerDateNewRecord);
        mGenderView = findViewById(R.id.spinnerGenderNewRecord);
        mWeightView = findViewById(R.id.editTextWeightNewRecord);
        mHeightFTView = findViewById(R.id.editTextHeightFtNewRecord);
        mHeightINView = findViewById(R.id.editTextHeightInchesNewRecord);
        mWeightGainView = findViewById(R.id.checkRecentWeightGainNewRecord);
        mWeightLossView = findViewById(R.id.checkRecentWeightLossNewRecord);
        mFatigueView = findViewById(R.id.checkFatigueNewRecord);
        mColdView = findViewById(R.id.checkColdNewRecord);
        mWeaknessView = findViewById(R.id.checkWeaknessNewRecord);
        mHairLossView = findViewById(R.id.checkHairLossNewRecord);
        mBloodPressureHighView = findViewById(R.id.editTextBloodPressureNewRecord);
        mBloodPressureLowView = findViewById(R.id.editTextBloodPressureLowNewRecord);
        mCholestrolView = findViewById(R.id.editTextCholestrolNewRecord);
        mT3View = findViewById(R.id.editTextT3NewRecord);
        mT4View = findViewById(R.id.editTextT4NewRecord);
        mTSHView = findViewById(R.id.editTextTSHNewRecord);

        ImageView imgSaveBtn = findViewById(R.id.img_save_button);
        imgSaveBtn.setOnClickListener(this);

        ImageButton imgBack = findViewById(R.id.toolBarButtonBack);
        imgBack.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_save_button:
                saveInfo();
                break;

            case R.id.toolBarButtonBack:
                goBack();
                break;
        }
    }

    private void saveInfo() {
        boolean isAllFeildDone = true;
        Sample mSample = new Sample();
        try {
            String mName = mNameView.getText().toString();

            if (!mName.isEmpty()&&mName.length()>0){
                mSample.setName(mName);
            }else {
                isAllFeildDone = false;
                mNameView.setError("Enter name");
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            mSample.setBirthDay(mDayView.getSelectedItemPosition() + 1);

        } catch (Exception e) {
            e.printStackTrace();
        }

        try {

            mSample.setBirthMonth(mMonthView.getSelectedItemPosition() + 1);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            if (mYearView.getText() != null && !mYearView.getText().toString().isEmpty() && mYearView.getText().toString().length() == 4) {
                mSample.setBirthYear(Integer.parseInt(mYearView.getText().toString()));
            } else {
                isAllFeildDone = false;
                mYearView.setError("Invalid year");
                Toast.makeText(this, "", Toast.LENGTH_SHORT).show();
                //mSample.setBirthYear(Validator.INVALID_SAMPLE_FIELD_INT_VALUE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            if (mGenderView.getSelectedItem().toString() != null)
                mSample.setGender(mGenderView.getSelectedItem().toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            if (Float.parseFloat(mWeightView.getText().toString()) > 0)
                mSample.setWeight(Float.parseFloat(mWeightView.getText().toString()));
        } catch (Exception e) {
            e.printStackTrace();
            mSample.setWeight(0);
        }

        try {
            if (Integer.parseInt(mHeightFTView.getText().toString()) > 0)
                mSample.setHeightFT(Integer.parseInt(mHeightFTView.getText().toString()));
        } catch (Exception e) {
            e.printStackTrace();
            mSample.setHeightFT(0);
        }

        try {

            mSample.setHeightIN(Integer.parseInt(mHeightINView.getText().toString()));
        } catch (Exception e) {
            e.printStackTrace();
            mSample.setHeightIN(0);
        }

        try {
            mSample.setSufferingFromWeightGain(mWeightGainView.isChecked());
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            mSample.setSufferingFromWeightLoss(mWeightLossView.isChecked());
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            mSample.setSufferingFromFatigue(mFatigueView.isChecked());
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            mSample.setSufferingFromFeelingCold(mColdView.isChecked());
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            mSample.setSufferingFromWeakness(mWeaknessView.isChecked());
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            mSample.setSufferingFromHairLoss(mHairLossView.isChecked());
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            mSample.setBpHigh(mBloodPressureHighView.getText().toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            mSample.setBpLow(mBloodPressureLowView.getText().toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            mSample.setCholestrol(mCholestrolView.getText().toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            mSample.setT3(mT3View.getText().toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            mSample.setT4(mT4View.getText().toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            mSample.setTsh(mTSHView.getText().toString());
        } catch (Exception e) {
            e.printStackTrace();
        }


        mSample.setRecordings(recordingArraylist);
        mSample.setTime(getCurrentDateTime());
        State.samples.add(mSample);

        if (isAllFeildDone){
            new SaveSample(mSample).execute();
        }


    }


    private String getCurrentDateTime() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return df.format(c.getTime());
    }


    public class SaveSample extends AsyncTask<String, String, String> {

        Sample sample = null;
        SaveSampleModel saveSampleModel;

        public SaveSample(Sample sample) {
            this.sample = sample;
        }

        @Override
        protected void onPreExecute() {
            Utility.myProgressBar.showProgressBar(InfoDetailsActivity.this);
        }

        @Override
        protected void onPostExecute(String response) {
            Utility.myProgressBar.hideProgressBar();
            new saveAudio(sample, String.valueOf(saveSampleModel.getData().getInsertId())).execute();

        }

        @Override
        protected String doInBackground(String... params) {
            String isError = "", response = "";
            HashMap<String, String> parameters = new HashMap<>();
            parameters.put("user_id", AppSharedPreferences.getInstance().getUserId());
            parameters.put("token", AppSharedPreferences.getInstance().getTokenValue());
            parameters.put("subject_details",sample.getSubjectDetails());
            parameters.put("sampleDetails", sample.getSampleDetails());
            parameters.put("symptomDetails", sample.getSymptomDetails());
            Log.e("InfoDetailsActivity ", "create_sample parameters : " + parameters);
            response = ApiCallFunction.postRequest(InfoDetailsActivity.this, Constant.BASE_URL+"create_sample",parameters);
            if (response.length() > 0) {
                Log.e("doInBackground: ", "create_sample Response : " + response);
                saveSampleModel = new Gson().fromJson(response, new TypeToken<SaveSampleModel>() {}.getType());
                Utility.myProgressBar.hideProgressBar();
            }
            return isError;
        }
    }


    public class saveAudio extends AsyncTask<String, String, String> {

        Sample sampleGetSet;
        String insertID;

        public saveAudio(Sample sample, String insertID) {
            this.sampleGetSet = sample;
            this.insertID = insertID;


        }

        @Override
        protected void onPreExecute() {
            Utility.myProgressBar.showProgressBar(InfoDetailsActivity.this);
        }

        @Override
        protected String doInBackground(String... params) {
            String isError = "";

             try {
                OkHttpClient client = new OkHttpClient.Builder().build();
                MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);

                for (int i =0;i<sampleGetSet.getRecordings().size();i++){
                    Recording recording = sampleGetSet.getRecordings().get(i);
                    int i1 = i + 1;
                    File file = new File(recording.getFilePath() + recording.getFileName());
                    if (file.exists()) {
                        builder.addFormDataPart("audio"+i1, String.valueOf(recording.getDuration()) + "_" + recording.getFileName(), RequestBody.create(MediaType.parse("text/plain"), file));

                    }
                }

                builder.addFormDataPart("user_id", AppSharedPreferences.getInstance().getUserId());
                builder.addFormDataPart("token",AppSharedPreferences.getInstance().getTokenValue());
                builder.addFormDataPart("subject_id",insertID);

                Request request = new Request.Builder().url(Constant.BASE_URL+"upload_audio_sample" ).post(builder.build()).build();

                Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onResponse(Call call, final Response response) {
                        Utility.myProgressBar.hideProgressBar();
                        String strResponse = null;
                        try {
                            strResponse = response.body().string();
                            Log.e("saveAudio ", " onResponse(): " + strResponse);

                            UploadAudioModel uploadAudioModel = new Gson().fromJson(strResponse, new TypeToken<UploadAudioModel>() {}.getType());
                            AppSharedPreferences.getInstance().setUploadAudioModel(uploadAudioModel);

                            if(uploadAudioModel.getIsError().equals(false)){

                                Intent i = new Intent(InfoDetailsActivity.this, MyAnalyzeResultActivity.class);
                                i.putExtra("InsertID", insertID);
                                i.putExtra("From","InfoDetailsActivity");

                                finish();
                                startActivity(i);
                                overridePendingTransition(R.anim.trans_left_in, R.anim.trans_left_out);
                            }


                        } catch (IOException e) {
                            e.printStackTrace();
                        }


                    }
                    @Override
                    public void onFailure(Call call, IOException e) {
                        Utility.myProgressBar.hideProgressBar();
                    }
                });
            } catch (Exception ignored) {
                Utility.myProgressBar.hideProgressBar();
            }

            return isError;
        }

        @Override
        protected void onPostExecute(String response) {


        }
    }

    @Override
    public void onBackPressed() {
        goBack();
    }

    private void goBack() {
        Intent intent = new Intent(this,HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP| Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        finish();
        startActivity(intent);
        overridePendingTransition(R.anim.trans_left_in, R.anim.trans_left_out);
    }
}
