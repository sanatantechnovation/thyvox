package com.healtharkinsights.thyvox.interfaces;

public interface DeleteSampleInterface {
    void deleteSampleCall(int position,String sampleId);

}
