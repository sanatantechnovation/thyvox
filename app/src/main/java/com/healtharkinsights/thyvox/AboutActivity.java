package com.healtharkinsights.thyvox;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class AboutActivity extends AppCompatActivity {

    private boolean showButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.WHITE);
        }
        setContentView(R.layout.activity_about);

        if (getIntent().hasExtra("showButton")){
            showButton = getIntent().getBooleanExtra("showButton", false);
        }

        RelativeLayout layoutNext = findViewById(R.id.layout_next);
        ImageView  imgBack = findViewById(R.id.img_back);
        TextView btnGoNext = findViewById(R.id.btn_go_next);

        if (showButton){
            layoutNext.setVisibility(View.VISIBLE);
            imgBack.setVisibility(View.GONE);
        }else {
            imgBack.setVisibility(View.VISIBLE);
            layoutNext.setVisibility(View.GONE);
        }

        btnGoNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AboutActivity.this, HomeActivity.class));
                finish();
                overridePendingTransition(R.anim.trans_left_in, R.anim.trans_left_out);
            }
        });

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                backButton();
            }
        });

    }

    @Override
    public void onBackPressed() {
        backButton();
    }

    private void backButton(){
        if (showButton){
            finish();
        }else {
            Intent intent = new Intent(AboutActivity.this,HomeActivity.class);
            finish();
            startActivity(intent);
            overridePendingTransition(R.anim.trans_left_in, R.anim.trans_left_out);
        }
    }
}
