package com.healtharkinsights.thyvox;

import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import com.healtharkinsights.thyvox.helper.Constant;
import com.healtharkinsights.thyvox.helper.ApiCallFunction;
import com.healtharkinsights.thyvox.helper.Utility;
import com.healtharkinsights.thyvox.model.RegisterModel;

import java.util.HashMap;

public class SignupActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.WHITE);
        }
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_signup);

        final EditText etEmail = findViewById(R.id.input_email);
        final EditText edPassword = findViewById(R.id.input_password);
        final EditText edName = findViewById(R.id.input_name);

        findViewById(R.id.btn_signup).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isAllDone = true;
                String email = etEmail.getText().toString();
                String password = edPassword.getText().toString();
                String name = edName.getText().toString();

                boolean validEmail = Utility.isEditTextContainEmail(email);

                if (!validEmail){
                    etEmail.setError("Enter valid email id");
                    isAllDone = false;
                }
                if (password.isEmpty()){
                    edPassword.setError("Enter Password");
                    isAllDone = false;
                }

                if (isAllDone){
                    new Register().execute(email,password,name);
                }

            }
        });

        findViewById(R.id.link_login).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SignupActivity.this, LoginActivity.class);
                finish();
                startActivity(intent);
                overridePendingTransition(R.anim.trans_left_in, R.anim.trans_left_out);
            }
        });
    }


    public class Register extends AsyncTask<String, String, String> {
        RegisterModel registerModel;

        @Override
        protected void onPreExecute() {
            Utility.myProgressBar.showProgressBar(SignupActivity.this);
        }

        @Override
        protected void onPostExecute(String response) {
            Utility.myProgressBar.hideProgressBar();
            Toast.makeText(SignupActivity.this, registerModel.getMessage(), Toast.LENGTH_SHORT).show();

        }

        @Override
        protected String doInBackground(String... params) {

            HashMap<String, String> parameters = new HashMap<>();
            parameters.put("email", params[0]);
            parameters.put("password",  params[1]);
            parameters.put("name", params[2]);

            Log.e( "registration: "," parameter : "+parameters.toString() );

            String isError = "",response = "";
            response = ApiCallFunction.postRequest(SignupActivity.this, Constant.BASE_URL+"register_user",parameters);
            if (response.length() > 0) {
                Log.e( "registration: "," response : "+response );
                registerModel = new Gson().fromJson(response, new TypeToken<RegisterModel>() {}.getType());
                if (registerModel.getIsError().equals(false)) {
                    Intent intent = new Intent(SignupActivity.this, LoginActivity.class);
                    finish();
                    startActivity(intent);
                    overridePendingTransition(R.anim.trans_left_in, R.anim.trans_left_out);
                }


            }
            return isError;
        }
    }

}
